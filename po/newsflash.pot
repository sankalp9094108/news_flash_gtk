# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the newsflash package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: newsflash\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-20 11:24+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/com.gitlab.newsflash.appdata.xml.in.in:7
#: data/com.gitlab.newsflash.desktop.in.in:3
#: data/resources/ui_templates/main_window.ui:6
msgid "NewsFlash"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:8
msgid "Follow your favorite blogs and news sites."
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:11
msgid ""
"NewsFlash is a program designed to complement an already existing web-based "
"RSS reader account."
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:15
msgid ""
"It combines all the advantages of web based services like syncing across all "
"your devices with everything you expect from a modern desktop program: "
"Desktop notifications, fast search and filtering, tagging, handy keyboard "
"shortcuts and having access to all your articles as long as you like."
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:60
#: data/com.gitlab.newsflash.appdata.xml.in.in:154
#: data/com.gitlab.newsflash.appdata.xml.in.in:174
#: data/com.gitlab.newsflash.appdata.xml.in.in:193
#: data/com.gitlab.newsflash.appdata.xml.in.in:207
#: data/com.gitlab.newsflash.appdata.xml.in.in:218
#: data/com.gitlab.newsflash.appdata.xml.in.in:230
#: data/com.gitlab.newsflash.appdata.xml.in.in:239
#: data/com.gitlab.newsflash.appdata.xml.in.in:252
#: data/com.gitlab.newsflash.appdata.xml.in.in:262
#: data/com.gitlab.newsflash.appdata.xml.in.in:302
msgid "Bugfixes:"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:62
msgid "feedbin: fix sync error"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:63
msgid "feedbin: fix thumbnails"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:64
msgid "feedbin: fix enclosures"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:65
msgid "small article visual fix"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:71
#: data/com.gitlab.newsflash.appdata.xml.in.in:165
#: data/com.gitlab.newsflash.appdata.xml.in.in:188
#: data/com.gitlab.newsflash.appdata.xml.in.in:291
msgid "Features:"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:73
msgid "Support for Inoreader"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:74
msgid "feedly no longer supported due to expired API key"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:76
#: data/com.gitlab.newsflash.appdata.xml.in.in:87
#: data/com.gitlab.newsflash.appdata.xml.in.in:96
#: data/com.gitlab.newsflash.appdata.xml.in.in:107
#: data/com.gitlab.newsflash.appdata.xml.in.in:125
#: data/com.gitlab.newsflash.appdata.xml.in.in:140
msgid "Bugfixes/Improvements:"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:78
msgid "NewsBlur: access denied on login"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:79
msgid "Mercury Parser: failed due to invalid string format"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:80
msgid "Startup parameters being ignored"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:81
msgid "Article View: add inspect menu entry"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:89
msgid "Discover: fix crash"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:90
msgid "Sidebar: Improve selection and iteration"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:98
msgid "Switch out remote Mercury parser for local one"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:99
msgid "Filter out socks4 proxies not supported by http library"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:100
msgid "Complete URLs in content with xml:base attribute"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:101
msgid "Local RSS: Use feed URL as ID if the feed doesn't provide one"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:109
msgid "Use OPML crate for ex/import"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:110
msgid "Indent OPML xml on export"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:111
msgid "Improve feedly sync behaviour"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:112
msgid "Local RSS: Handle relative URLs of enclosures"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:113
msgid "Enclosures now opening from withing flatpak sandbox"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:119
msgid ""
"This release brings support for thumbnails in the article list and "
"enclosures for articles."
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:124
msgid ""
"NewsFlash is now able to download and display thumbnails if the service or "
"feed provides them."
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:127
msgid "Feed-List: Feeds showing even parent category was collapsed"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:128
msgid "Feed-List: Relevant feed filter not working"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:129
msgid "Feed-List: Drag and Drop not working"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:135
msgid "Hotfix miniflux sync behaviour"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:142
msgid "Fix foreign key constraint upgrading database scheme"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:143
msgid "Miniflux: better sync"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:144
msgid "Arrow-key navigation in sidebar"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:145
msgid "Improve article-list updates"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:146
msgid "Fix \"keep articles\" 1 week setting"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:147
msgid "Clarify the \"keep articles\" setting"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:153
msgid "Hotfix for broken Database migration"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:156
msgid "Database migration: copy old article data"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:157
msgid ""
"local RSS: avoid reassigning current date when no date information is "
"available"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:158
msgid "GUI: fix offline/online popover positioning"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:159
msgid "GUI: re-enable arrow navigation in article-list"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:167
msgid "Support for NewsBlur"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:168
msgid "feedly: port to new collections API"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:169
msgid "Option to limit duration articles are kept in the database"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:170
msgid "Option to hide feeds without unread/starred articles"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:171
msgid "Update article with new content like forum posts"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:172
msgid "local RSS: support for MediaRSS"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:176
msgid "Fix regression in database that lead to foreign key constraints failing"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:177
msgid "feedly: fix token not refreshing"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:178
msgid "Remove unnecessary menu entry"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:179
msgid "Various updated libraries and small code improvements"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:190
msgid "migrate to libhandy1"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:191
msgid "GUI can now be squeezed to 360px horizontally to better fit mobile"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:195
msgid "local RSS: don't fail on invalid xml attributes"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:196
msgid "local RSS: unique feed ID for feeds without useful data"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:197
msgid "miniflux: parsing article date when timezone is set to UTC"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:198
msgid "fever: sync with articles without feed"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:199
msgid "feedly: sync with missing stream title"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:200
msgid "feedly: sync with duplicate tags field of entry"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:201
msgid "unable to re-add feed after deletion"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:209
msgid "local RSS: enconding support"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:210
msgid "local RSS: support content:encoded for RSS 1.0"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:211
msgid "database: issue when deleting feed-mappings"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:212
msgid "feedly: ignore negative published timestamps"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:220
msgid "feedly: don't require fingerprint"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:221
msgid "feedly: don't require origin title"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:222
msgid "enable gzip feature of reqwest client"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:223
msgid "local RSS: smarter selection of article link"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:224
msgid "keybinding: fix next-item"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:232
msgid "web login page invisible"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:233
msgid "OPML import containing feeds without category"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:241
msgid "Article List: Load more articles even when scrolling very fast"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:242
msgid "feedbin: don't require entries to have a summary"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:243
msgid "feedbin: reset cache after a sync failed"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:244
msgid "feedbin: ignore articles without feed"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:245
msgid "fever: fix issue with tt-rss fever plugin"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:246
msgid "Import atom feeds from OPML exported with liferea"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:254
msgid "Close about dialog with no headerbar"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:255
msgid "Don't trigger shortcuts twice when article view is in focus"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:256
msgid "Firefox issues when opening URL with it"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:264
msgid "Updated App Icon"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:265
msgid "Set webview background color according to Gtk theme"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:266
msgid ""
"Allow flatpak to access 'Downloads' to save images from the article view"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:267
msgid "Remove article view content justification"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:268
msgid "React to dark theme preference set from outside"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:269
msgid "Add \"busy timeout\" for database connections"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:270
msgid "Fever: escape HTML entities in title"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:271
msgid "feedly: ignore articles without existing feed"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:272
msgid "feedly: make stream_id optional"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:273
msgid "OPML import: use `No Title` if both `title` and `text` are missing"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:274
msgid "OPML import: always parse feed if no website is available"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:275
msgid "OPML import: Spin the update button to indicate long processing"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:276
msgid "Article list: don't insert row when already contained in model"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:277
msgid ""
"Toggle article state based of article list selection instead of article view"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:278
msgid "Convert timestamp into local timezone for display"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:279
msgid "Fix 32 bit build"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:280
msgid "Fix copying text by not inhibiting all key presses in webview"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:281
msgid ""
"Fix appicon not properly shown: don't call show_all() in application "
"constructor"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:282
msgid "Update date string in article list if a new day has begun"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:283
msgid "Correctly show initial state of article theme setting"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:284
msgid "Crash when opening and using settings a second time"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:285
msgid "Unreadable links in Gruvbox theme"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:293
msgid "Feedbin support"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:294
msgid "Mercury Parser hosted by Feedbin as fallback for the internal scraper"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:295
msgid "Readability as a fallback for the internal scraper"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:296
msgid "Support for uncategorized feeds and \"generated/fake\" categories"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:297
msgid "Save images to Downloads folder"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:298
msgid "Switch between scraped and original content"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:299
msgid "Search full text of scraped article"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:304
msgid "Fever: accepting both string and integer for 'last_refreshed_on_time'"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:305
msgid "Fever: crash when syncing uncategorized feeds"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:306
msgid "Fever: can't login with HTTP basic auth"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:307
msgid "Fever: slow bulk updating of articles"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:308
msgid "Local: feeds from the same website could overwrite each other"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:309
msgid "Local: Improve layout when only summary is present"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:310
msgid "Local: better handle misformed dates in ATOM feeds"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:311
msgid "First sync downloads too many articles"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:312
msgid "Can't apply tag to article after creating it"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:313
msgid "Better error handling for non-web logins"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:314
msgid "UI elements visible that shouldn't"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:315
msgid "Problems opening links from articles"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:316
msgid "Next article shortcut skipping articles"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:317
msgid "Debug files written to disk"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:318
msgid "Improved right-to-left language support"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:319
msgid "Prefer scraped content when exporting an article"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:324
msgid "First public release"
msgstr ""

#: data/com.gitlab.newsflash.appdata.xml.in.in:331
msgid "Jan Lukas Gernert"
msgstr ""

#: data/com.gitlab.newsflash.desktop.in.in:4
msgid "RSS Reader"
msgstr ""

#: data/com.gitlab.newsflash.desktop.in.in:5
msgid ""
"Faster than flash to deliver you the latest news from your subscribed feeds"
msgstr ""

#: data/com.gitlab.newsflash.desktop.in.in:11
msgid "Gnome;GTK;RSS;Feed;"
msgstr ""

#: data/resources/ui_templates/account_widget.ui:17
#: data/resources/ui_templates/password_login.ui:36
#: data/resources/ui_templates/web_login.ui:46
msgid "label"
msgstr ""

#: data/resources/ui_templates/add_category_widget.ui:18
msgid "Add a Category"
msgstr ""

#: data/resources/ui_templates/add_category_widget.ui:28
msgid "Enter the Title of the new Category"
msgstr ""

#: data/resources/ui_templates/add_category_widget.ui:36
msgid "Category Title"
msgstr ""

#: data/resources/ui_templates/add_category_widget.ui:43
#: data/resources/ui_templates/add_feed_widget.ui:215
#: data/resources/ui_templates/add_popover.ui:38
#: data/resources/ui_templates/add_tag_widget.ui:45
msgid "Add"
msgstr ""

#: data/resources/ui_templates/add_feed_widget.ui:27
msgid "Add a Feed"
msgstr ""

#: data/resources/ui_templates/add_feed_widget.ui:37
msgid "Enter the URL of a feed or website"
msgstr ""

#: data/resources/ui_templates/add_feed_widget.ui:45
msgid "Website or Feed URL"
msgstr ""

#: data/resources/ui_templates/add_feed_widget.ui:52
#: data/resources/ui_templates/add_feed_widget.ui:124
msgid "Next"
msgstr ""

#: data/resources/ui_templates/add_feed_widget.ui:82
msgid "Multiple Feeds found"
msgstr ""

#: data/resources/ui_templates/add_feed_widget.ui:92
msgid "Select one of the listed feeds below"
msgstr ""

#: data/resources/ui_templates/add_feed_widget.ui:121
msgid "text"
msgstr ""

#: data/resources/ui_templates/add_feed_widget.ui:132
#: data/resources/ui_templates/article_list_column.ui:45
#: data/resources/ui_templates/article_list_column.ui:91
#: data/resources/ui_templates/articleview_column.ui:40
#: data/resources/ui_templates/articleview_column.ui:169
#: data/resources/ui_templates/articleview_column.ui:255
#: data/resources/ui_templates/sidebar_column.ui:52
msgid "spinner"
msgstr ""

#: data/resources/ui_templates/add_feed_widget.ui:168
msgid "Add Feed"
msgstr ""

#: data/resources/ui_templates/add_feed_widget.ui:178
msgid "Edit the title of the feed and set its category"
msgstr ""

#: data/resources/ui_templates/add_feed_widget.ui:201
#: data/resources/ui_templates/feedlist_item.ui:36
msgid "Feed Title"
msgstr ""

#: data/resources/ui_templates/add_popover.ui:61
msgid "Feed"
msgstr ""

#: data/resources/ui_templates/add_popover.ui:87
msgid "Category"
msgstr ""

#: data/resources/ui_templates/add_popover.ui:113
msgid "Tag"
msgstr ""

#: data/resources/ui_templates/add_tag_widget.ui:18
msgid "Add a Tag"
msgstr ""

#: data/resources/ui_templates/add_tag_widget.ui:28
msgid "Enter the Title of the new Tag"
msgstr ""

#: data/resources/ui_templates/add_tag_widget.ui:38
#: data/resources/ui_templates/popover_tag.ui:32
#: data/resources/ui_templates/tag.ui:30
msgid "Tag Title"
msgstr ""

#: data/resources/ui_templates/article.ui:48
msgid "feed"
msgstr ""

#: data/resources/ui_templates/article.ui:63
msgid "/"
msgstr ""

#: data/resources/ui_templates/article.ui:77
msgid "date"
msgstr ""

#: data/resources/ui_templates/article.ui:124
#: data/resources/ui_templates/article.ui:125
msgid "title"
msgstr ""

#: data/resources/ui_templates/article.ui:142
msgid "No Summary"
msgstr ""

#: data/resources/ui_templates/article_list_column.ui:32
#: data/resources/ui_templates/article_list_column.ui:78
#: data/resources/ui_templates/articleview_column.ui:26
#: data/resources/ui_templates/articleview_column.ui:154
#: data/resources/ui_templates/articleview_column.ui:241
#: data/resources/ui_templates/sidebar_column.ui:39
msgid "button"
msgstr ""

#: data/resources/ui_templates/article_list_column.ui:37
#: data/resources/ui_templates/sidebar_column.ui:44
msgid "Refresh Content"
msgstr ""

#: data/resources/ui_templates/article_list_column.ui:61
#: data/resources/ui_templates/sidebar_column.ui:68
msgid "Go back to online mode"
msgstr ""

#: data/resources/ui_templates/article_list_column.ui:68
msgid "Search Articles"
msgstr ""

#: data/resources/ui_templates/article_list_column.ui:83
msgid "Set All/Feed/Category as read"
msgstr ""

#: data/resources/ui_templates/article_list_column.ui:129
msgid "All"
msgstr ""

#: data/resources/ui_templates/article_list_column.ui:139
msgid "Unread"
msgstr ""

#: data/resources/ui_templates/article_list_column.ui:149
msgid "Starred"
msgstr ""

#: data/resources/ui_templates/article_view.ui:43
msgid "crash"
msgstr ""

#: data/resources/ui_templates/article_view.ui:56
msgid "WebKit has crashed"
msgstr ""

#: data/resources/ui_templates/article_view.ui:81
msgid "empty"
msgstr ""

#: data/resources/ui_templates/article_view.ui:96
msgid "article"
msgstr ""

#: data/resources/ui_templates/article_view_url.ui:16
msgid "URL"
msgstr ""

#: data/resources/ui_templates/articleview_column.ui:22
msgid "Article Menu"
msgstr ""

#: data/resources/ui_templates/articleview_column.ui:67
#: data/resources/ui_templates/settings_shortcuts.ui:71
msgid "Toggle Starred"
msgstr ""

#: data/resources/ui_templates/articleview_column.ui:75
msgid "unmarked"
msgstr ""

#: data/resources/ui_templates/articleview_column.ui:86
msgid "marked"
msgstr ""

#: data/resources/ui_templates/articleview_column.ui:105
#: data/resources/ui_templates/settings_shortcuts.ui:52
msgid "Toggle Read"
msgstr ""

#: data/resources/ui_templates/articleview_column.ui:113
msgid "read"
msgstr ""

#: data/resources/ui_templates/articleview_column.ui:124
msgid "unread"
msgstr ""

#: data/resources/ui_templates/articleview_column.ui:161
#: data/resources/ui_templates/articleview_column.ui:247
msgid "Try to show full content"
msgstr ""

#: data/resources/ui_templates/articleview_column.ui:187
#: data/resources/ui_templates/articleview_column.ui:228
msgid "Tag Article"
msgstr ""

#: data/resources/ui_templates/articleview_column.ui:199
#: data/resources/ui_templates/articleview_column.ui:272
msgid "Enclosures"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:21
msgid "Discover"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:59
msgid "Search by #topic, website or RSS link"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:66
msgid "English"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:67
msgid "German"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:82
msgid "page0"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:92
msgid "Featured Topics"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:113
msgid "#News"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:127
msgid "#Tech"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:140
msgid "#Science"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:153
msgid "#Culture"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:168
msgid "#Media"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:183
msgid "#Sports"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:198
msgid "#Food"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:213
msgid "#Open source"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:231
msgid "page1"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:243
msgid "Search Results"
msgstr ""

#: data/resources/ui_templates/discover_dialog.ui:314
msgid "No Results"
msgstr ""

#: data/resources/ui_templates/discover_search_item.ui:39
msgid "Title"
msgstr ""

#: data/resources/ui_templates/discover_search_item.ui:51
msgid "Description"
msgstr ""

#: data/resources/ui_templates/error_detail_dialog.ui:18
msgid "Error Details"
msgstr ""

#: data/resources/ui_templates/feedlist_item.ui:50
#: data/resources/ui_templates/settings_app_page.ui:100
#: data/resources/ui_templates/settings_app_page.ui:124
#: data/resources/ui_templates/sidebar.ui:56
msgid "0"
msgstr ""

#: data/resources/ui_templates/keybind_editor.ui:41
msgid "Enter new keybinding to change"
msgstr ""

#: data/resources/ui_templates/keybind_editor.ui:52
msgid "STUFF"
msgstr ""

#: data/resources/ui_templates/keybind_editor.ui:65
msgid "Press ESC to cancel or Backspace to disable the keybinding."
msgstr ""

#: data/resources/ui_templates/keybind_editor.ui:78
msgid "SHORTCUT"
msgstr ""

#: data/resources/ui_templates/keybind_editor.ui:91
msgid "Set"
msgstr ""

#: data/resources/ui_templates/password_login.ui:48
msgid "Ignore"
msgstr ""

#: data/resources/ui_templates/password_login.ui:54
#: data/resources/ui_templates/reset_page.ui:37
#: data/resources/ui_templates/web_login.ui:34
msgid "Details"
msgstr ""

#: data/resources/ui_templates/password_login.ui:73
msgid "Headline"
msgstr ""

#: data/resources/ui_templates/password_login.ui:100
msgid "Url:"
msgstr ""

#: data/resources/ui_templates/password_login.ui:112
#: data/resources/ui_templates/password_login.ui:210
msgid "Username:"
msgstr ""

#: data/resources/ui_templates/password_login.ui:124
#: data/resources/ui_templates/password_login.ui:222
msgid "Password:"
msgstr ""

#: data/resources/ui_templates/password_login.ui:197
msgid "HTTP Basic Auth"
msgstr ""

#: data/resources/ui_templates/password_login.ui:281
msgid "Login"
msgstr ""

#: data/resources/ui_templates/rename_dialog.ui:15
#: data/resources/ui_templates/rename_dialog.ui:41
msgid "Rename"
msgstr ""

#: data/resources/ui_templates/reset_page.ui:51
msgid "Resetting Account failed"
msgstr ""

#: data/resources/ui_templates/reset_page.ui:80
msgid "Reset"
msgstr ""

#: data/resources/ui_templates/service_row.ui:30
msgid "Service Name"
msgstr ""

#: data/resources/ui_templates/service_row.ui:81
msgid "Price:"
msgstr ""

#: data/resources/ui_templates/service_row.ui:97
msgid "License:"
msgstr ""

#: data/resources/ui_templates/service_row.ui:113
msgid "Service Type:"
msgstr ""

#: data/resources/ui_templates/service_row.ui:129
msgid "Website:"
msgstr ""

#: data/resources/ui_templates/service_row.ui:152
msgid "http://example.com"
msgstr ""

#: data/resources/ui_templates/service_row.ui:166
msgid "self hosted"
msgstr ""

#: data/resources/ui_templates/service_row.ui:179
msgid "GPLv3"
msgstr ""

#: data/resources/ui_templates/service_row.ui:192
msgid "free"
msgstr ""

#: data/resources/ui_templates/settings_app_page.ui:12
msgid "Keep running"
msgstr ""

#: data/resources/ui_templates/settings_app_page.ui:13
msgid "Fetch updates in the background"
msgstr ""

#: data/resources/ui_templates/settings_app_page.ui:25
msgid "Sync on startup"
msgstr ""

#: data/resources/ui_templates/settings_app_page.ui:26
msgid "Fetch updates when the App is launched"
msgstr ""

#: data/resources/ui_templates/settings_app_page.ui:43
msgid "Manual"
msgstr ""

#: data/resources/ui_templates/settings_app_page.ui:44
msgid "No automatic synchronization"
msgstr ""

#: data/resources/ui_templates/settings_app_page.ui:55
msgid "Synchronize every"
msgstr ""

#: data/resources/ui_templates/settings_app_page.ui:67
msgid "Custom Interval"
msgstr ""

#: data/resources/ui_templates/settings_app_page.ui:77
msgid "hh:mm:ss"
msgstr ""

#: data/resources/ui_templates/settings_app_page.ui:92
msgid "User Data"
msgstr ""

#: data/resources/ui_templates/settings_app_page.ui:108
msgid "Keep Articles"
msgstr ""

#: data/resources/ui_templates/settings_app_page.ui:109
msgid "Refers to the time of sync"
msgstr ""

#: data/resources/ui_templates/settings_app_page.ui:116
msgid "Cache"
msgstr ""

#: data/resources/ui_templates/settings_app_page.ui:132
msgid "Webview"
msgstr ""

#: data/resources/ui_templates/settings_app_page.ui:140
msgid "Clear"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:14
msgid "Next Article"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:33
msgid "Previous Article"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:90
msgid "Open URL"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:115
msgid "Next Item"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:134
msgid "Previous Item"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:153
msgid "Expand / Collapse"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:172
msgid "Mark selected read"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:197
#: src/content_page/sidebar_column.rs:180
msgid "Shortcuts"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:216
msgid "Refresh"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:235
msgid "Search"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:254
#: src/content_page/sidebar_column.rs:182
msgid "Quit"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:273
#: data/resources/ui_templates/sidebar.ui:45
msgid "All Articles"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:292
msgid "Only Unread"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:311
msgid "Only Starred"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:336
msgid "Scroll up"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:355
msgid "Scroll down"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:374
msgid "Scrape article content"
msgstr ""

#: data/resources/ui_templates/settings_shortcuts.ui:393
msgid "Tag article"
msgstr ""

#: data/resources/ui_templates/settings_views.ui:14
msgid "Only show relevant items"
msgstr ""

#: data/resources/ui_templates/settings_views.ui:15
msgid "Hide feeds and categories without unread/unstarred items"
msgstr ""

#: data/resources/ui_templates/settings_views.ui:37
msgid "Order"
msgstr ""

#: data/resources/ui_templates/settings_views.ui:42
msgid "Show Thumbnails"
msgstr ""

#: data/resources/ui_templates/settings_views.ui:43
msgid "Only if available"
msgstr ""

#: data/resources/ui_templates/settings_views.ui:65
msgid "Theme"
msgstr ""

#: data/resources/ui_templates/settings_views.ui:89
#: src/settings/dialog/theme_chooser.rs:139
msgid "Default"
msgstr ""

#: data/resources/ui_templates/settings_views.ui:104
msgid "Allow selection"
msgstr ""

#: data/resources/ui_templates/settings_views.ui:119
msgid "Use System Font"
msgstr ""

#: data/resources/ui_templates/settings_views.ui:134
msgid "Font"
msgstr ""

#: data/resources/ui_templates/sidebar.ui:95
msgid "Categories"
msgstr ""

#: data/resources/ui_templates/sidebar.ui:145
msgid "Tags"
msgstr ""

#: data/resources/ui_templates/sidebar_column.ui:22
msgid "Account"
msgstr ""

#: data/resources/ui_templates/sidebar_column.ui:76
msgid "Menu"
msgstr ""

#: data/resources/ui_templates/sidebar_column.ui:83
msgid "Add Feed/Category/Tag"
msgstr ""

#: data/resources/ui_templates/tag_popover.ui:32
msgid "Add Tags"
msgstr ""

#: data/resources/ui_templates/tag_popover.ui:40
msgid "Type to create a new Tag"
msgstr ""

#: data/resources/ui_templates/tag_popover.ui:64
msgid "Create Tag"
msgstr ""

#: data/resources/ui_templates/tag_popover.ui:104
msgid "Tag the Article"
msgstr ""

#: data/resources/ui_templates/tag_popover.ui:118
msgid "Press enter to assign a Tag or ctrl + enter to create a new Tag"
msgstr ""

#: data/resources/ui_templates/tag_popover.ui:173
msgid "Search and Create Tags"
msgstr ""

#: data/resources/ui_templates/welcome_page.ui:36
msgid "Where are your feeds?"
msgstr ""

#: data/resources/ui_templates/welcome_page.ui:49
msgid "Please select the RSS service you are using and log in to get going."
msgstr ""

#: src/app.rs:1175 src/app.rs:1602
msgid "_Save"
msgstr ""

#: src/app.rs:1176 src/app.rs:1506 src/app.rs:1603
msgid "_Cancel"
msgstr ""

#: src/app.rs:1177 src/content_page/articleview_column.rs:236
msgid "Export Article"
msgstr ""

#: src/app.rs:1505
msgid "_Open"
msgstr ""

#: src/app.rs:1507 src/app.rs:1604 src/content_page/sidebar_column.rs:186
msgid "Export OPML"
msgstr ""

#: src/app.rs:2536
msgid "New Articles"
msgstr ""

#: src/app.rs:2539
msgid "There is 1 new article ({} unread)"
msgstr ""

#: src/app.rs:2542
msgid "There are {} new articles ({} unread)"
msgstr ""

#: src/rename_dialog.rs:89
msgid "Empty name not allowed"
msgstr ""

#: src/add_popover/add_feed_widget.rs:175
msgid "No valid URL."
msgstr ""

#: src/add_popover/add_feed_widget.rs:498
msgid "No Feed found."
msgstr ""

#: src/article_list/mod.rs:508
msgid "No articles that fit \"{}\"."
msgstr ""

#: src/article_list/mod.rs:509
msgid "No articles."
msgstr ""

#: src/article_list/mod.rs:512
msgid "No unread articles that fit \"{}\"."
msgstr ""

#: src/article_list/mod.rs:513
msgid "No unread articles."
msgstr ""

#: src/article_list/mod.rs:516
msgid "No starred articles that fit \"{}\"."
msgstr ""

#: src/article_list/mod.rs:517
msgid "No starred articles."
msgstr ""

#: src/article_list/mod.rs:527
msgid "No articles that fit \"{}\" in {} \"{}\"."
msgstr ""

#: src/article_list/mod.rs:528
msgid "No articles in {} \"{}\"."
msgstr ""

#: src/article_list/mod.rs:532
msgid "No unread articles that fit \"{}\" in {} \"{}\"."
msgstr ""

#: src/article_list/mod.rs:535
msgid "No unread articles in {} \"{}\"."
msgstr ""

#: src/article_list/mod.rs:539
msgid "No starred articles that fit \"{}\" in {} \"{}\"."
msgstr ""

#: src/article_list/mod.rs:542
msgid "No starred articles in {} \"{}\"."
msgstr ""

#: src/article_list/mod.rs:548
msgid "No articles that fit \"{}\" in tag \"{}\"."
msgstr ""

#: src/article_list/mod.rs:549
msgid "No articles in tag \"{}\"."
msgstr ""

#: src/article_list/mod.rs:552
msgid "No unread articles that fit \"{}\" in tag \"{}\"."
msgstr ""

#: src/article_list/mod.rs:553
msgid "No unread articles in tag \"{}\"."
msgstr ""

#: src/article_list/mod.rs:556
msgid "No starred articles that fit \"{}\" in tag \"{}\"."
msgstr ""

#: src/article_list/mod.rs:557
msgid "No starred articles in tag \"{}\"."
msgstr ""

#: src/content_page/articleview_column.rs:238
msgid "Open in browser"
msgstr ""

#: src/content_page/articleview_column.rs:241
msgid "Close Article"
msgstr ""

#: src/content_page/mod.rs:215
msgid "Deleted Category '{}'"
msgstr ""

#: src/content_page/mod.rs:216
msgid "Deleted Feed '{}'"
msgstr ""

#: src/content_page/mod.rs:217
msgid "Deleted Tag '{}'"
msgstr ""

#: src/content_page/sidebar_column.rs:181
msgid "About"
msgstr ""

#: src/content_page/sidebar_column.rs:185
msgid "Import OPML"
msgstr ""

#: src/content_page/sidebar_column.rs:189
msgid "Preferences"
msgstr ""

#: src/content_page/sidebar_column.rs:190
msgid "Discover Feeds"
msgstr ""

#: src/login_screen/password_login.rs:185
msgid "Please log into {} and enjoy using NewsFlash"
msgstr ""

#: src/login_screen/password_login.rs:344
msgid "HTTP authentication required."
msgstr ""

#: src/login_screen/password_login.rs:347
msgid "No valid CA certificate available."
msgstr ""

#: src/login_screen/password_login.rs:351
msgid "Could not log in"
msgstr ""

#: src/login_screen/password_login.rs:357 src/login_screen/web_login.rs:127
msgid "Unknown error."
msgstr ""

#: src/login_screen/web_login.rs:126
msgid "Failed to log in"
msgstr ""

#: src/settings/dialog/keybinding_editor.rs:117
msgid "Disable Keybinding"
msgstr ""

#: src/settings/dialog/keybinding_editor.rs:134
msgid "Illegal Keybinding"
msgstr ""

#: src/settings/dialog/theme_chooser.rs:147
msgid "Spring"
msgstr ""

#: src/settings/dialog/theme_chooser.rs:155
msgid "Midnight"
msgstr ""

#: src/settings/dialog/theme_chooser.rs:163
msgid "Parchment"
msgstr ""

#: src/settings/dialog/theme_chooser.rs:171
msgid "Gruvbox"
msgstr ""

#: src/welcome_screen/service_row.rs:111
msgid "No Website"
msgstr ""

#: src/welcome_screen/service_row.rs:114
msgid "Unknown Free License"
msgstr ""

#: src/welcome_screen/service_row.rs:115
msgid "Proprietary Software"
msgstr ""

#: src/welcome_screen/service_row.rs:129
msgid "Local data only"
msgstr ""

#: src/welcome_screen/service_row.rs:132
msgid "Synced and self-hosted"
msgstr ""

#: src/welcome_screen/service_row.rs:134
msgid "Synced"
msgstr ""

#: src/welcome_screen/service_row.rs:141
msgid "Free"
msgstr ""

#: src/welcome_screen/service_row.rs:142
msgid "Paid"
msgstr ""

#: src/welcome_screen/service_row.rs:143
msgid "Free / Paid Premium"
msgstr ""
