use crate::{
    app::{Action, App},
    util::Util,
};
use glib::{clone, subclass};
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, Inhibit, Switch, Widget};
use libadwaita::{subclass::prelude::*, EntryRow, PreferencesPage};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/settings_share.ui")]
    pub struct SettingsSharePage {
        #[template_child]
        pub share_pocket_switch: TemplateChild<Switch>,
        #[template_child]
        pub share_instapaper_switch: TemplateChild<Switch>,
        #[template_child]
        pub share_twitter_switch: TemplateChild<Switch>,
        #[template_child]
        pub share_mastodon_switch: TemplateChild<Switch>,
        #[template_child]
        pub share_reddit_switch: TemplateChild<Switch>,
        #[template_child]
        pub share_telegram_switch: TemplateChild<Switch>,

        #[template_child]
        pub share_custom_switch: TemplateChild<Switch>,
        #[template_child]
        pub share_custom_name_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub share_custom_url_entry: TemplateChild<EntryRow>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SettingsSharePage {
        const NAME: &'static str = "SettingsSharePage";
        type ParentType = PreferencesPage;
        type Type = super::SettingsSharePage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SettingsSharePage {}

    impl WidgetImpl for SettingsSharePage {}

    impl PreferencesPageImpl for SettingsSharePage {}
}

glib::wrapper! {
    pub struct SettingsSharePage(ObjectSubclass<imp::SettingsSharePage>)
        @extends Widget, PreferencesPage;
}

impl SettingsSharePage {
    pub fn new() -> Self {
        glib::Object::new::<Self>()
    }

    pub fn init(&self) {
        let imp = self.imp();

        imp.share_pocket_switch
            .set_state(App::default().settings().read().get_share_pocket_enabled());
        imp.share_instapaper_switch
            .set_state(App::default().settings().read().get_share_instapaper_enabled());
        imp.share_twitter_switch
            .set_state(App::default().settings().read().get_share_twitter_enabled());
        imp.share_mastodon_switch
            .set_state(App::default().settings().read().get_share_mastodon_enabled());
        imp.share_reddit_switch
            .set_state(App::default().settings().read().get_share_reddit_enabled());
        imp.share_telegram_switch
            .set_state(App::default().settings().read().get_share_telegram_enabled());

        let custom_share_enabled = App::default().settings().read().get_share_custom_enabled();
        imp.share_custom_switch.set_state(custom_share_enabled);
        imp.share_custom_name_entry.set_sensitive(custom_share_enabled);
        imp.share_custom_url_entry.set_sensitive(custom_share_enabled);

        if let Some(name) = App::default().settings().read().get_share_custom_name() {
            imp.share_custom_name_entry.set_text(name);
        }
        if let Some(url) = App::default().settings().read().get_share_custom_url() {
            imp.share_custom_url_entry.set_text(url);
        }

        imp.share_pocket_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_share_pocket_enabled(is_set)
                .is_err()
            {
                Util::send(Action::SimpleMessage("Failed to set setting 'pocket enabled'.".into()));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
            Inhibit(false)
        });

        imp.share_instapaper_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_share_instapaper_enabled(is_set)
                .is_err()
            {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'instapaper enabled'.".into(),
                ));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
            Inhibit(false)
        });

        imp.share_twitter_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_share_twitter_enabled(is_set)
                .is_err()
            {
                Util::send(Action::SimpleMessage("Failed to set setting 'twitter enabled'.".into()));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
            Inhibit(false)
        });

        imp.share_mastodon_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_share_mastodon_enabled(is_set)
                .is_err()
            {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'mastodon enabled'.".into(),
                ));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
            Inhibit(false)
        });

        imp.share_reddit_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_share_reddit_enabled(is_set)
                .is_err()
            {
                Util::send(Action::SimpleMessage("Failed to set setting 'reddit enabled'.".into()));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
            Inhibit(false)
        });

        imp.share_telegram_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_share_telegram_enabled(is_set)
                .is_err()
            {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'telegram enabled'.".into(),
                ));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
            Inhibit(false)
        });

        imp.share_custom_switch.connect_state_set(clone!(
            @weak self as this => @default-panic, move |_switch, is_set| {

            let imp = this.imp();
            imp.share_custom_name_entry.set_sensitive(is_set);
            imp.share_custom_url_entry.set_sensitive(is_set);

            if App::default()
                .settings()
                .write()
                .set_share_custom_enabled(is_set)
                .is_err()
            {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'custom share enabled'.".into(),
                ));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
            Inhibit(false)
        }));

        imp.share_custom_name_entry.connect_text_notify(|entry| {
            if App::default()
                .settings()
                .write()
                .set_share_custom_name(Some(entry.text().as_str().into()))
                .is_err()
            {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'custom share name'.".into(),
                ));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
        });

        imp.share_custom_url_entry.connect_text_notify(|entry| {
            if App::default()
                .settings()
                .write()
                .set_share_custom_url(Some(entry.text().as_str().into()))
                .is_err()
            {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'custom share url'.".into(),
                ));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
        });
    }
}
