use crate::article_view::{ArticleTheme, ArticleView};
use crate::i18n::i18n;
use chrono::Utc;
use glib::{clone, subclass::*};
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, GestureClick, ListBox, ListBoxRow, Popover, Widget};
use news_flash::models::{ArticleID, FatArticle, FeedID, Marked, Read};
use webkit2gtk::{traits::WebViewExt, WebView};

mod imp {
    use super::*;
    use glib::subclass;
    use once_cell::sync::Lazy;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/theme_chooser.ui")]
    pub struct ThemeChooser {
        #[template_child]
        pub theme_list: TemplateChild<ListBox>,

        #[template_child]
        pub default_row: TemplateChild<ListBoxRow>,
        #[template_child]
        pub default_click_controller: TemplateChild<GestureClick>,
        #[template_child]
        pub default_view: TemplateChild<WebView>,

        #[template_child]
        pub spring_row: TemplateChild<ListBoxRow>,
        #[template_child]
        pub spring_click_controller: TemplateChild<GestureClick>,
        #[template_child]
        pub spring_view: TemplateChild<WebView>,

        #[template_child]
        pub midnight_row: TemplateChild<ListBoxRow>,
        #[template_child]
        pub midnight_click_controller: TemplateChild<GestureClick>,
        #[template_child]
        pub midnight_view: TemplateChild<WebView>,

        #[template_child]
        pub parchment_row: TemplateChild<ListBoxRow>,
        #[template_child]
        pub parchment_click_controller: TemplateChild<GestureClick>,
        #[template_child]
        pub parchment_view: TemplateChild<WebView>,

        #[template_child]
        pub gruvbox_row: TemplateChild<ListBoxRow>,
        #[template_child]
        pub gruvbox_click_controller: TemplateChild<GestureClick>,
        #[template_child]
        pub gruvbox_view: TemplateChild<WebView>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ThemeChooser {
        const NAME: &'static str = "ThemeChooser";
        type ParentType = Popover;
        type Type = super::ThemeChooser;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ThemeChooser {
        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
                vec![Signal::builder("article-theme-changed")
                    .param_types([ArticleTheme::static_type()])
                    .build()]
            });
            SIGNALS.as_ref()
        }

        fn constructed(&self) {
            let mut demo_article = FatArticle {
                article_id: ArticleID::new("demo"),
                title: None,
                author: None,
                feed_id: FeedID::new("demo_feed"),
                direction: None,
                date: Utc::now().naive_utc(),
                synced: Utc::now().naive_utc(),
                marked: Marked::Unmarked,
                unread: Read::Unread,
                url: None,
                summary: None,
                html: None,
                scraped_content: None,
                plain_text: None,
                thumbnail_url: None,
            };

            super::ThemeChooser::prepare_theme_selection(
                &*self.default_row,
                &self.default_click_controller,
                &self.default_view,
                &mut demo_article,
                ArticleTheme::Default,
                &i18n("Default"),
            );
            super::ThemeChooser::prepare_theme_selection(
                &*self.spring_row,
                &self.spring_click_controller,
                &self.spring_view,
                &mut demo_article,
                ArticleTheme::Spring,
                &i18n("Spring"),
            );
            super::ThemeChooser::prepare_theme_selection(
                &*self.midnight_row,
                &self.midnight_click_controller,
                &self.midnight_view,
                &mut demo_article,
                ArticleTheme::Midnight,
                &i18n("Midnight"),
            );
            super::ThemeChooser::prepare_theme_selection(
                &*self.parchment_row,
                &self.parchment_click_controller,
                &self.parchment_view,
                &mut demo_article,
                ArticleTheme::Parchment,
                &i18n("Parchment"),
            );
            super::ThemeChooser::prepare_theme_selection(
                &*self.gruvbox_row,
                &self.gruvbox_click_controller,
                &self.gruvbox_view,
                &mut demo_article,
                ArticleTheme::Gruvbox,
                &i18n("Gruvbox"),
            );

            self.theme_list
                .connect_row_activated(clone!(@weak self as this => @default-panic, move |_list, row| {
                    this.obj().popdown();

                    let theme = ArticleTheme::from_str(row.widget_name().as_str());

                    this.obj().emit_by_name::<()>("article-theme-changed", &[&theme]);
                }));
        }
    }

    impl WidgetImpl for ThemeChooser {}

    impl PopoverImpl for ThemeChooser {}
}

glib::wrapper! {
    pub struct ThemeChooser(ObjectSubclass<imp::ThemeChooser>)
        @extends Widget, Popover;
}

impl ThemeChooser {
    pub fn new() -> Self {
        glib::Object::new::<Self>()
    }

    fn prepare_theme_selection(
        row: &ListBoxRow,
        click_controller: &GestureClick,
        view: &WebView,
        article: &mut FatArticle,
        theme: ArticleTheme,
        name: &str,
    ) {
        click_controller.connect_released(clone!(@weak row => @default-panic, move |_gesture, times, _x, _y| {
            if times != 1 {
                return;
            }
            row.emit_activate();
        }));
        article.title = Some(name.to_owned());
        let prefer_dark_theme = libadwaita::StyleManager::default().is_dark();
        ArticleView::load_style_sheet(view);
        let html = ArticleView::build_article_static(
            "theme_preview",
            article,
            "Feed Name",
            None,
            Some(theme),
            Some(10240),
            Some(false),
            false,
            prefer_dark_theme,
        );
        view.load_html(&html, None);
    }
}
