use crate::article_view::ArticleTheme;
use serde::{Deserialize, Serialize};
use std::default::Default;

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct ArticleViewSettings {
    pub theme: ArticleTheme,
    pub allow_select: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub font: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub content_width: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub line_height: Option<f32>,
}
