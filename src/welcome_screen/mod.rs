mod service_row;

use self::service_row::ServiceRow;
use crate::app::App;
use crate::login_screen::LoginPrevPage;
use glib::clone;
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{CompositeTemplate, ListBox};
use news_flash::models::{LoginData, LoginGUI, PluginID, PluginInfo};
use news_flash::NewsFlash;
use parking_lot::RwLock;
use std::collections::HashMap;
use std::sync::Arc;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/welcome_page.ui")]
    pub struct WelcomePage {
        pub services: Arc<RwLock<HashMap<i32, (PluginID, LoginGUI)>>>,

        #[template_child]
        pub list: TemplateChild<ListBox>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WelcomePage {
        const NAME: &'static str = "WelcomePage";
        type ParentType = gtk4::Box;
        type Type = super::WelcomePage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for WelcomePage {}

    impl WidgetImpl for WelcomePage {}

    impl BoxImpl for WelcomePage {}
}

glib::wrapper! {
    pub struct WelcomePage(ObjectSubclass<imp::WelcomePage>)
        @extends gtk4::Widget, gtk4::Box;
}

impl WelcomePage {
    pub fn new() -> Self {
        glib::Object::new::<Self>()
    }
    pub fn init(&self) {
        self.populate();
        self.connect_signals();
    }

    fn populate(&self) {
        let services = NewsFlash::list_backends();
        let imp = self.imp();

        for (index, (id, plugin_info)) in services.into_iter().enumerate() {
            let row = ServiceRow::new();
            row.init(&plugin_info);
            imp.list.insert(&row, index as i32);
            let PluginInfo {
                id: _,
                name: _,
                icon: _,
                icon_symbolic: _,
                website: _,
                service_type: _,
                license_type: _,
                service_price: _,
                login_gui,
            } = plugin_info;
            imp.services.write().insert(index as i32, (id, login_gui));
        }
    }

    fn connect_signals(&self) {
        let imp = self.imp();

        imp.list.connect_row_activated(
            clone!(@strong imp.services as services => @default-panic, move |_list, row| {
                if let Some((id, login_desc)) = services.read().get(&row.index()) {
                    match login_desc {
                        LoginGUI::OAuth(_) | LoginGUI::Direct(_) => {
                            App::default().main_window().show_login_page(&id, None, LoginPrevPage::Welcome)
                        }
                        LoginGUI::None => {
                            App::default().login(LoginData::None(id.clone()));
                        }
                    };
                }
            }),
        );
    }
}
