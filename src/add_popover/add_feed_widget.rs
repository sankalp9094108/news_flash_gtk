use crate::app::{Action, App};
use crate::i18n::i18n;
use crate::util::{GtkUtil, Util, CHANNEL_ERROR, RUNTIME_ERROR};
use futures::channel::oneshot;
use futures::future::FutureExt;
use glib::{clone, subclass::*, types::Type};
use gtk4::{
    prelude::*, subclass::prelude::*, Box, Button, ComboBox, CompositeTemplate, Entry, Image, Label, ListBox,
    ListBoxRow, ListStore, Orientation, Stack, Widget,
};
use news_flash::{error::FeedParserError, ParsedUrl};
use pango::EllipsizeMode;
use parking_lot::RwLock;
use std::sync::Arc;
use tokio::runtime::Runtime;

use news_flash::models::{Category, CategoryID, FavIcon, Feed, FeedID, Url};

pub const NEW_CATEGORY_ICON: &str = "folder-new-symbolic";
pub const WARN_ICON: &str = "dialog-warning-symbolic";

#[derive(Clone, Debug)]
pub enum AddCategory {
    New(String),
    Existing(CategoryID),
    None,
}

mod imp {
    use super::*;
    use glib::subclass;
    use once_cell::sync::Lazy;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/add_feed_widget.ui")]
    pub struct AddFeedWidget {
        #[template_child]
        pub stack: TemplateChild<Stack>,
        #[template_child]
        pub url_entry: TemplateChild<Entry>,
        #[template_child]
        pub parse_button: TemplateChild<Button>,
        #[template_child]
        pub feed_list: TemplateChild<ListBox>,
        #[template_child]
        pub select_button: TemplateChild<Button>,
        #[template_child]
        pub select_button_stack: TemplateChild<Stack>,
        #[template_child]
        pub feed_title_entry: TemplateChild<Entry>,
        #[template_child]
        pub favicon_image: TemplateChild<Image>,
        #[template_child]
        pub add_button: TemplateChild<Button>,
        #[template_child]
        pub category_combo: TemplateChild<ComboBox>,

        pub feed_url: Arc<RwLock<Option<Url>>>,
        pub feed_category: Arc<RwLock<AddCategory>>,
        pub categories: Arc<RwLock<Vec<Category>>>,
    }

    impl Default for AddFeedWidget {
        fn default() -> Self {
            AddFeedWidget {
                stack: TemplateChild::default(),
                url_entry: TemplateChild::default(),
                parse_button: TemplateChild::default(),
                feed_list: TemplateChild::default(),
                select_button: TemplateChild::default(),
                select_button_stack: TemplateChild::default(),
                feed_title_entry: TemplateChild::default(),
                favicon_image: TemplateChild::default(),
                add_button: TemplateChild::default(),
                category_combo: TemplateChild::default(),

                feed_url: Arc::new(RwLock::new(None)),
                feed_category: Arc::new(RwLock::new(AddCategory::None)),
                categories: Arc::new(RwLock::new(Vec::new())),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AddFeedWidget {
        const NAME: &'static str = "AddFeedWidget";
        type ParentType = Box;
        type Type = super::AddFeedWidget;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AddFeedWidget {
        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| vec![Signal::builder("feed-added").build()]);
            SIGNALS.as_ref()
        }

        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for AddFeedWidget {}

    impl BoxImpl for AddFeedWidget {}
}

glib::wrapper! {
    pub struct AddFeedWidget(ObjectSubclass<imp::AddFeedWidget>)
        @extends Widget, Box;
}

impl AddFeedWidget {
    pub fn new() -> Self {
        glib::Object::new::<Self>()
    }

    fn init(&self) {
        let imp = self.imp();

        // setup category-combo entry
        let feed_category_entry = imp.category_combo.child().unwrap().downcast::<Entry>().unwrap();
        feed_category_entry.set_placeholder_text(Some("Category"));
        feed_category_entry.set_secondary_icon_name(None);
        feed_category_entry.set_secondary_icon_activatable(false);
        feed_category_entry.set_secondary_icon_tooltip_text(Some("Create new Category"));

        self.query_categories();

        // make parse button sensitive if entry contains text and vice versa
        let parse_button = imp.parse_button.get();
        imp.url_entry
            .connect_changed(clone!(@weak parse_button => @default-panic, move |entry| {
                if entry.text().as_str().is_empty() {
                    parse_button.set_sensitive(false);
                } else {
                    parse_button.set_sensitive(true);
                }

                entry.set_secondary_icon_name(None);
                entry.set_secondary_icon_tooltip_text(None);
            }));

        // hit enter in entry to parse url
        let parse_button = imp.parse_button.get();
        imp.url_entry
            .connect_activate(clone!(@weak parse_button => @default-panic, move |_entry| {
                if parse_button.get_sensitive() {
                    parse_button.emit_clicked();
                }
            }));

        // parse url and switch to feed selection or final page
        imp.parse_button.connect_clicked(clone!(
            @weak self as widget => @default-panic, move |_button|
        {
            let imp = widget.imp();

            let mut url_text = imp.url_entry.text().as_str().to_owned();
            if !url_text.starts_with("http://") && !url_text.starts_with("https://") {
                url_text.insert_str(0, "https://");
            }
            if let Ok(url) = Url::parse(&url_text) {
                widget.parse_feed_url(&url);
            } else {
                log::error!("No valid url: '{}'", url_text);
                imp.url_entry.set_secondary_icon_name(Some(WARN_ICON));
                imp.url_entry.set_secondary_icon_tooltip_text(Some(&i18n("No valid URL.")));
            }
        }));

        // make add_button sensitive / insensitive
        feed_category_entry.connect_changed(clone!(
            @weak self as widget => @default-panic, move |entry|
        {
            let imp = widget.imp();

            let folder_icon = if imp.category_combo.active_id().is_some() {
                if let Some(id) = imp.category_combo.active_id() {
                    let category_id = CategoryID::new(id.as_str());
                    *imp.feed_category.write() = AddCategory::Existing(category_id);
                }
                None
            } else if entry.text().is_empty() {
                *imp.feed_category.write() = AddCategory::None;
                None
            } else if imp.categories.read().iter().any(|c| c.label == entry.text().as_str()) {
                let category_id = imp.categories
                    .read()
                    .iter()
                    .find(|c| c.label == entry.text().as_str())
                    .map(|c| c.category_id.clone());

                if let Some(category_id) = category_id {
                    *imp.feed_category.write() = AddCategory::Existing(category_id);
                }
                None
            } else {
                *imp.feed_category.write() = AddCategory::New(entry.text().as_str().into());
                Some(NEW_CATEGORY_ICON)
            };

            entry.set_secondary_icon_name(folder_icon);
        }));

        let add_button = imp.add_button.get();
        imp.feed_title_entry.connect_changed(
            clone!(@weak add_button, @strong feed_category_entry => @default-panic, move |entry| {
                add_button.set_sensitive(!entry.text().as_str().is_empty());
            }),
        );

        imp.add_button.connect_clicked(clone!(
            @weak self as widget => @default-panic, move |_button|
        {
            let imp = widget.imp();

            let feed_url = match imp.feed_url.read().clone() {
                Some(url) => url,
                None => {
                    log::error!("Failed to add feed: No valid url");
                    Util::send(Action::SimpleMessage("Failed to add feed".into()));
                    return;
                }
            };
            let feed_title = if imp.feed_title_entry.text().as_str().is_empty() { None } else { Some(imp.feed_title_entry.text().as_str().into()) };
            let feed_category = imp.feed_category.read().clone();

            Util::send(Action::AddFeed((feed_url, feed_title, feed_category)));
            widget.emit_by_name::<()>("feed-added", &[]);
        }));
    }

    fn query_categories(&self) {
        let imp = self.imp();

        let mut categories = if let Some(news_flash) = App::default().news_flash().read().as_ref() {
            match news_flash.get_categories() {
                Ok((categories, _category_mappings)) => categories,
                Err(_error) => Vec::new(),
            }
        } else {
            Vec::new()
        };

        let list_store = ListStore::new(&[Type::STRING, Type::STRING]);

        for category in &categories {
            let iter = list_store.append();
            list_store.set(
                &iter,
                &[(0, &Some(category.category_id.as_str())), (1, &category.label)],
            );
        }

        imp.category_combo.set_model(Some(&list_store));
        imp.categories.write().clear();
        imp.categories.write().append(&mut categories);
    }

    pub fn reset(&self) {
        let imp = self.imp();

        imp.stack.set_visible_child_name("url_page");
        imp.url_entry.set_text("");
        imp.feed_title_entry.set_text("");
        imp.category_combo.set_active_id(None);
        let feed_category_entry = imp.category_combo.child().unwrap().downcast::<Entry>().unwrap();
        feed_category_entry.set_text("");
        while let Some(row) = imp.feed_list.first_child() {
            imp.feed_list.remove(&row);
        }
        imp.feed_url.write().take();
        *imp.feed_category.write() = AddCategory::None;
    }

    fn fill_mupliple_feed_list(&self, feed_vec: Vec<(String, Url)>) {
        let imp = self.imp();

        let select_button = imp.select_button.get();
        imp.feed_list.connect_row_selected(move |_list, row| {
            select_button.set_sensitive(row.is_some());
        });

        imp.select_button.connect_clicked(clone!(@weak self as widget => @default-panic, move |button|
        {
            let imp = widget.imp();

            if let Some(row) = imp.feed_list.selected_row() {
                imp.select_button_stack.set_visible_child_name("spinner");
                button.set_sensitive(false);

                let url = Url::parse(row.widget_name().as_str()).expect("should never fail since it comes from 'url.as_str()'");
                let feed_id = FeedID::new(url.as_str());

                let (sender, receiver) = oneshot::channel::<Option<ParsedUrl>>();

                let thread_future = async move {
                    let result = Runtime::new()
                        .expect(RUNTIME_ERROR)
                        .block_on(news_flash::feed_parser::download_and_parse_feed(
                            &url, &feed_id, None, &Util::build_client(),
                        ))
                        .ok();
                    sender.send(result).expect(CHANNEL_ERROR);
                };

                let glib_future = receiver.map(clone!(
                    @weak widget => @default-panic, move |res|
                {
                    let imp = widget.imp();

                    if let Some(ParsedUrl::SingleFeed(feed)) = res.expect(CHANNEL_ERROR) {
                        widget.fill_feed_page(feed);
                        imp.stack.set_visible_child_name("parsed_feed_page");
                    } else if let Some(child) = row.child() {
                        if let Ok(_box) = child.downcast::<Box>() {
                            if let Some(icon) = _box.first_child().and_then(|child| child.next_sibling()) {
                                icon.set_visible(true);
                            }
                        }
                    }

                    imp.select_button_stack.set_visible_child_name("text");
                    imp.select_button.set_sensitive(true);
                }));

                App::default().threadpool().spawn_ok(thread_future);
                Util::glib_spawn_future(glib_future);
            }
        }));
        for (title, url) in feed_vec {
            let label = Label::new(Some(&title));
            label.set_size_request(0, 50);
            label.set_ellipsize(EllipsizeMode::End);
            label.set_xalign(0.0);

            let gtk_box = Box::new(Orientation::Horizontal, 0);
            gtk_box.set_margin_start(20);
            gtk_box.set_margin_end(20);
            gtk_box.prepend(&label);

            let row = ListBoxRow::new();

            let select_button = imp.select_button.get();
            row.connect_activate(move |_row| {
                select_button.activate();
            });

            row.set_selectable(true);
            row.set_activatable(false);
            row.set_widget_name(url.as_str());
            row.set_child(Some(&gtk_box));
            imp.feed_list.insert(&row, -1);
        }
    }

    fn fill_feed_page(&self, feed: Feed) {
        let imp = self.imp();

        imp.feed_title_entry.set_text(&feed.label);
        if let Some(new_feed_url) = &feed.feed_url {
            imp.feed_url.write().replace(new_feed_url.clone());
        } else {
            imp.feed_url.write().take();
        }

        let (sender, receiver) = oneshot::channel::<Option<FavIcon>>();

        let feed_clone = feed.clone();
        let thread_future =
            async move {
                let result = Runtime::new().expect(RUNTIME_ERROR).block_on(
                    news_flash::util::favicon_cache::FavIconCache::scrap(&feed_clone, &Util::build_client()),
                );
                sender.send(result).expect(CHANNEL_ERROR);
            };

        let scale = GtkUtil::get_scale(&*imp.favicon_image);

        let favicon_image = imp.favicon_image.get();
        let glib_future = receiver.map(move |res| {
            if let Some(favicon) = res.expect(CHANNEL_ERROR) {
                if let Some(data) = &favicon.data {
                    if let Ok(texture) = GtkUtil::create_texture_from_bytes(data, 64, 64, scale) {
                        favicon_image.set_from_paintable(Some(&texture));
                    }
                }
            } else if let Some(icon_url) = feed.icon_url {
                let (sender, receiver) = oneshot::channel::<Option<Vec<u8>>>();

                let thread_future = async move {
                    let res = Runtime::new().expect(RUNTIME_ERROR).block_on(async {
                        match Util::build_client().get(icon_url.as_str()).send().await {
                            Ok(response) => match response.bytes().await {
                                Ok(bytes) => Some(Vec::from(bytes.as_ref())),
                                Err(_) => None,
                            },
                            Err(_) => None,
                        }
                    });
                    sender.send(res).expect(CHANNEL_ERROR);
                };

                let glib_future = receiver.map(move |res| {
                    if let Some(byte_vec) = res.expect(CHANNEL_ERROR) {
                        if let Ok(surface) = GtkUtil::create_texture_from_bytes(&byte_vec, 64, 64, scale) {
                            favicon_image.set_from_paintable(Some(&surface));
                        }
                    }
                });

                App::default().threadpool().spawn_ok(thread_future);
                Util::glib_spawn_future(glib_future);
            }
        });

        App::default().threadpool().spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    pub fn parse_feed_url(&self, url: &Url) {
        let imp = self.imp();
        let (sender, receiver) = oneshot::channel::<Result<ParsedUrl, FeedParserError>>();

        self.query_categories();

        // set 'next' button insensitive and show spinner
        imp.stack.set_visible_child_name("spinner");
        imp.parse_button.set_sensitive(false);

        let feed_id = FeedID::new(url.as_str());
        let thread_url = url.clone();
        let thread_future = async move {
            let result =
                Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash::feed_parser::download_and_parse_feed(
                        &thread_url,
                        &feed_id,
                        None,
                        &Util::build_client(),
                    ));
            sender.send(result).expect(CHANNEL_ERROR);
        };

        let glib_future = receiver.map(clone!(
            @weak self as widget,
            @strong url => @default-panic, move |res|
        {
            let imp = widget.imp();

            // parse url
            match res.expect(CHANNEL_ERROR) {
                Ok(result) => match result {
                    ParsedUrl::MultipleFeeds(feed_vec) => {
                        // url has multiple feeds: show selection page and list them there
                        imp.stack.set_visible_child_name("multiple_selection_page");
                        widget.fill_mupliple_feed_list(feed_vec);
                    }
                    ParsedUrl::SingleFeed(feed) => {
                        // url has single feed: move to feed page
                        imp.stack.set_visible_child_name("parsed_feed_page");
                        widget.fill_feed_page(feed);
                    }
                },
                Err(error) => {
                    log::error!("No feed found for url '{}': {}", url, error);
                    imp.stack.set_visible_child_name("url_page");
                    imp.url_entry.set_text(&url.to_string());
                    imp.url_entry.set_secondary_icon_name(Some(WARN_ICON));
                    imp.url_entry.set_secondary_icon_tooltip_text(Some(&i18n("No Feed found.")));
                }
            }

            // set 'next' buton sensitive again
            imp.parse_button.set_sensitive(true);
        }));

        App::default().threadpool().spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }
}
