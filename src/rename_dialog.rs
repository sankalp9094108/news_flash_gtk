use crate::i18n::i18n;
use crate::sidebar::{models::SidebarSelection, FeedListItemID};
use glib::{clone, IsA};
use gtk4::{prelude::*, subclass::prelude::*, Button, CompositeTemplate, Entry, Widget, Window};
use libadwaita::{subclass::prelude::AdwWindowImpl, Window as AdwWindow, WindowTitle};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/rename_dialog.ui")]
    pub struct RenameDialog {
        #[template_child]
        pub title: TemplateChild<WindowTitle>,
        #[template_child]
        pub rename_button: TemplateChild<Button>,
        #[template_child]
        pub rename_entry: TemplateChild<Entry>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for RenameDialog {
        const NAME: &'static str = "RenameDialog";
        type Type = super::RenameDialog;
        type ParentType = AdwWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for RenameDialog {}

    impl WidgetImpl for RenameDialog {}

    impl WindowImpl for RenameDialog {}

    impl AdwWindowImpl for RenameDialog {}
}

glib::wrapper! {
    pub struct RenameDialog(ObjectSubclass<imp::RenameDialog>)
        @extends Widget, Window;
}

impl RenameDialog {
    pub fn new<W: IsA<Window>>(parent: &W, item: &SidebarSelection) -> Self {
        let dialog: RenameDialog = glib::Object::new();
        let imp = dialog.imp();

        match item {
            SidebarSelection::All => {}
            SidebarSelection::FeedList(id, _) => match id {
                FeedListItemID::Feed(..) => imp.title.set_title("Rename Feed"),
                FeedListItemID::Category(_) => imp.title.set_title("Rename Category"),
            },
            _ => unimplemented!(),
        }

        imp.rename_entry.set_text(match item {
            SidebarSelection::All => "",
            SidebarSelection::FeedList(_, name) => name,
            _ => unimplemented!(),
        });

        imp.rename_entry
            .connect_changed(clone!(@weak dialog as this => @default-panic, move |entry| {
                let imp = this.imp();

                if entry.text().as_str().is_empty() {
                    imp.rename_button.set_sensitive(false);
                    entry.style_context().add_class("warning");
                    entry.set_secondary_icon_name(Some("dialog-warning-symbolic"));
                    entry.set_secondary_icon_tooltip_text(Some(&i18n("Empty name not allowed")));
                } else {
                    imp.rename_button.set_sensitive(true);
                    entry.style_context().remove_class("warning");
                    entry.set_secondary_icon_name(None);
                    entry.set_secondary_icon_tooltip_text(None);
                }
            }));

        dialog.set_transient_for(Some(parent));
        dialog.present();
        dialog
    }

    pub fn text(&self) -> String {
        self.imp().rename_entry.text().as_str().to_owned()
    }

    pub fn connect_rename<F: Fn(&Self) + 'static>(&self, f: F) {
        self.imp()
            .rename_button
            .connect_clicked(clone!(@weak self as this => @default-panic, move |_button| {
                f(&this);
            }));
    }
}
