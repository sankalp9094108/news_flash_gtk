use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate};
use gtk4::{Align, Box, Label, Widget};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/article_view_url.ui")]
    pub struct UrlOverlay {
        #[template_child]
        pub label: TemplateChild<Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for UrlOverlay {
        const NAME: &'static str = "UrlOverlay";
        type ParentType = Box;
        type Type = super::UrlOverlay;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for UrlOverlay {}

    impl WidgetImpl for UrlOverlay {}

    impl BoxImpl for UrlOverlay {}
}

glib::wrapper! {
    pub struct UrlOverlay(ObjectSubclass<imp::UrlOverlay>)
        @extends Widget, Box;
}

impl UrlOverlay {
    pub fn new() -> Self {
        glib::Object::new::<Self>()
    }

    pub fn set_url(&self, uri: &str, align: Align) {
        let imp = self.imp();

        let mut uri = uri.to_owned();
        let max_length = 45;
        if uri.chars().count() > max_length {
            uri = uri.chars().take(max_length).collect::<String>();
            uri.push_str("...");
        }

        imp.label.set_label(&uri);
        imp.label.set_width_chars(uri.chars().count() as i32 - 5);
        imp.label.set_halign(align);
    }

    pub fn reveal(&self, show: bool) {
        if show {
            self.show();
        } else {
            self.hide();
        }
    }
}
