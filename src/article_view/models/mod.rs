use glib::Enum;
use serde::{Deserialize, Serialize};

#[derive(Clone, Default, Debug, Serialize, Deserialize, Eq, PartialEq, Copy, Enum)]
#[repr(u32)]
#[enum_type(name = "NewsFlashArticleTheme")]
pub enum ArticleTheme {
    #[default]
    Default,
    Spring,
    Midnight,
    Parchment,
    Gruvbox,
}

impl ArticleTheme {
    pub fn from_str(string: &str) -> Self {
        match string {
            "theme dark" | "theme default" | "dark" | "default" => Self::Default,
            "spring" | "theme spring" => Self::Spring,
            "midnight" | "theme midnight" => Self::Midnight,
            "parchment" | "theme parchment" => Self::Parchment,
            "gruvbox" | "theme gruvbox" => Self::Gruvbox,

            _ => Self::Default,
        }
    }
    pub fn to_str(&self, prefer_dark_theme: bool) -> &str {
        match self {
            Self::Default => {
                if prefer_dark_theme {
                    "theme dark"
                } else {
                    "theme default"
                }
            }
            Self::Spring => "theme spring",
            Self::Midnight => "theme midnight",
            Self::Parchment => "theme parchment",
            Self::Gruvbox => "theme gruvbox",
        }
    }

    pub fn name(&self) -> &str {
        match self {
            Self::Default => "Default",
            Self::Spring => "Spring",
            Self::Midnight => "Midnight",
            Self::Parchment => "Parchment",
            Self::Gruvbox => "Gruvbox",
        }
    }
}
