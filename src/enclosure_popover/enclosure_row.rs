use gdk4::AppLaunchContext;
use gio::AppInfo;
use glib::clone;
use gtk4::{prelude::*, subclass::prelude::*, Button, CompositeTemplate, Widget};
use libadwaita::{prelude::*, subclass::prelude::*, ActionRow, PreferencesRow};
use news_flash::models::Enclosure;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/enclosure_row.ui")]
    pub struct EnclosureRow {
        #[template_child]
        pub invis_button: TemplateChild<Button>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for EnclosureRow {
        const NAME: &'static str = "EnclosureRow";
        type ParentType = ActionRow;
        type Type = super::EnclosureRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for EnclosureRow {}

    impl WidgetImpl for EnclosureRow {}

    impl ListBoxRowImpl for EnclosureRow {}

    impl ActionRowImpl for EnclosureRow {}

    impl PreferencesRowImpl for EnclosureRow {}
}

glib::wrapper! {
    pub struct EnclosureRow(ObjectSubclass<imp::EnclosureRow>)
        @extends Widget, PreferencesRow, ActionRow;
}

impl EnclosureRow {
    pub fn new() -> Self {
        glib::Object::new::<Self>()
    }

    pub fn init(&self, enclosure: &Enclosure, title: &str) {
        let imp = self.imp();

        if let Some(title) = &enclosure.title {
            self.set_title(title);
        } else {
            self.set_title(title);
        }

        let icon_name = if let Some(mime) = enclosure.mime_type.as_ref() {
            if mime.starts_with("image") {
                "image-x-generic-symbolic"
            } else if mime.starts_with("video") {
                "video-x-generic-symbolic"
            } else if mime.starts_with("audio") {
                "audio-x-generic-symbolic"
            } else {
                "globe-alt-symbolic"
            }
        } else {
            "globe-alt-symbolic"
        };

        let url_string = enclosure.url.to_string();
        let url_shortened = url_string.chars().take(35).chain("...".chars()).collect::<String>();

        self.set_icon_name(Some(icon_name));
        self.set_subtitle(&url_shortened);
        self.set_tooltip_text(Some(&url_string));

        imp.invis_button.connect_activate(clone!(
            @strong enclosure => @default-panic, move |_button| {
                let url_str = enclosure.url.as_str();

                // FIXME: hope at some point opening default app for mime-type will be possible with desktop-portal
                // https://github.com/flatpak/xdg-desktop-portal/issues/574
                if let Err(error) = AppInfo::launch_default_for_uri(url_str, None::<&AppLaunchContext>) {
                    log::warn!("Failed to launch default application for uri: '{}', {}", enclosure.url, error);
                }

                // if let Some(mime_type) = &enclosure.mime_type.as_ref() {
                //     if let Some(app_info) = AppInfo::get_default_for_type(mime_type, true) {
                //         let url = vec![url_str];
                //         if app_info.launch_uris(&url, None::<&AppLaunchContext>).is_err() {
                //             log::warn!("Failed to launch default application for uri: '{}'", enclosure.url);
                //         }
                //     } else {
                //         log::warn!("No default application that handles uris for mime type: '{:?}'", enclosure.mime_type);
                //     }
                // } else {
                //     if let Err(error) = open::that(&url_str) {
                //         log::error!("Failed to open enclosure url '{}': '{}'", url_str, error);
                //     }
                // }
        }));
    }
}
