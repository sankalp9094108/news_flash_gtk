use super::article_tags::ArticleTags;
use super::models::{ArticleGObject, GDateTime, GMarked, GRead, GTags};
use crate::app::App;
use crate::util::{GtkUtil, Util};
use chrono::Utc;
use futures::channel::oneshot;
use futures::future::FutureExt;
use gdk4::Paintable;
use gio::{Menu, MenuItem};
use glib::{
    clone, subclass::*, ParamSpec, ParamSpecEnum, ParamSpecString, SignalHandlerId, StaticType, ToValue, Value,
};
use gtk4::{
    prelude::*, subclass::prelude::*, CompositeTemplate, GestureClick, GestureLongPress, Image, Label, PopoverMenu,
    Revealer,
};
use log::warn;
use news_flash::models::{ArticleID, FavIcon, FeedID, Thumbnail};
use news_flash::util::text2html::Text2Html;
use once_cell::sync::Lazy;
use parking_lot::RwLock;
use std::cell::{Cell, RefCell};
use std::ops::Deref;
use std::sync::Arc;

static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
    vec![Signal::builder("activated")
        .param_types([ArticleRow::static_type()])
        .build()]
});

mod imp {
    use gdk4::glib::ParamSpecBoxed;

    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/article.ui")]
    pub struct ArticleRow {
        #[template_child]
        pub title_label: TemplateChild<Label>,
        #[template_child]
        pub summary_label: TemplateChild<Label>,
        #[template_child]
        pub feed_label: TemplateChild<Label>,
        #[template_child]
        pub date_label: TemplateChild<Label>,
        #[template_child]
        pub favicon: TemplateChild<Image>,
        #[template_child]
        pub thumb_image: TemplateChild<Image>,
        #[template_child]
        pub thumb_revealer: TemplateChild<Revealer>,
        #[template_child]
        pub marked: TemplateChild<Image>,
        #[template_child]
        pub row_click: TemplateChild<GestureClick>,
        #[template_child]
        pub row_activate: TemplateChild<GestureClick>,
        #[template_child]
        pub row_long_press: TemplateChild<GestureLongPress>,
        #[template_child]
        pub article_tags: TemplateChild<ArticleTags>,

        pub article_id: RwLock<ArticleID>,
        pub feed_id: RwLock<FeedID>,
        pub g_read: Cell<GRead>,
        pub g_marked: Cell<GMarked>,
        pub g_date: Cell<GDateTime>,
        pub g_tags: RefCell<GTags>,

        pub row_hovered: RwLock<bool>,
        pub popover: Arc<RwLock<Option<PopoverMenu>>>,
        pub right_click_source: RwLock<Option<SignalHandlerId>>,
        pub long_press_source: RwLock<Option<SignalHandlerId>>,
    }

    impl Default for ArticleRow {
        fn default() -> Self {
            ArticleRow {
                title_label: TemplateChild::default(),
                summary_label: TemplateChild::default(),
                feed_label: TemplateChild::default(),
                date_label: TemplateChild::default(),
                favicon: TemplateChild::default(),
                thumb_image: TemplateChild::default(),
                thumb_revealer: TemplateChild::default(),
                marked: TemplateChild::default(),
                row_click: TemplateChild::default(),
                row_activate: TemplateChild::default(),
                row_long_press: TemplateChild::default(),
                article_tags: TemplateChild::default(),

                article_id: RwLock::new(ArticleID::new("")),
                feed_id: RwLock::new(FeedID::new("")),
                g_read: Cell::new(GRead::Unread),
                g_marked: Cell::new(GMarked::Unmarked),
                g_date: Cell::new(Utc::now().naive_utc().into()),
                g_tags: RefCell::new(Vec::new().into()),

                row_hovered: RwLock::new(false),
                popover: Arc::new(RwLock::new(None)),
                right_click_source: RwLock::new(None),
                long_press_source: RwLock::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleRow {
        const NAME: &'static str = "ArticleRow";
        type ParentType = gtk4::Box;
        type Type = super::ArticleRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ArticleRow {
        fn signals() -> &'static [Signal] {
            SIGNALS.as_ref()
        }

        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecEnum::builder::<GRead>("read").build(),
                    ParamSpecEnum::builder::<GMarked>("marked").build(),
                    ParamSpecString::builder("date").build(),
                    ParamSpecBoxed::builder::<GTags>("tags").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "read" => {
                    let input: GRead = value.get().expect("The value needs to be of type `GRead`.");
                    self.g_read.set(input);

                    self.obj().update_title_label_style();
                }
                "marked" => {
                    let input: GMarked = value.get().expect("The value needs to be of type `GMarked`.");
                    self.g_marked.replace(input);

                    self.obj().update_marked();
                }
                "date" => {
                    let input: String = value.get().expect("The value needs to be of type `string`.");
                    self.date_label.set_text(&input);
                }
                "tags" => {
                    let input = value.get().expect("The value needs to be of type `GTags`.");
                    self.g_tags.replace(input);

                    self.obj().update_tags();
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "read" => self.g_read.get().to_value(),
                "marked" => self.g_marked.get().to_value(),
                "date" => self.date_label.text().as_str().to_string().to_value(),
                "tags" => self.g_tags.borrow().to_value(),
                _ => unimplemented!(),
            }
        }
    }

    impl WidgetImpl for ArticleRow {}

    impl BoxImpl for ArticleRow {}
}

glib::wrapper! {
    pub struct ArticleRow(ObjectSubclass<imp::ArticleRow>)
        @extends gtk4::Widget, gtk4::Box;
}

impl ArticleRow {
    pub fn new() -> Self {
        let row = glib::Object::new::<Self>();
        row.init();
        row
    }

    fn init(&self) {
        let imp = self.imp();
        imp.marked.set_from_icon_name(Some("starred-symbolic"));

        imp.row_activate.connect_released(clone!(
            @weak self as this => @default-panic, move |_gesture, times, _x, _y|
        {
            if times != 1 {
                return
            }

            this.activate();
        }));
    }

    pub fn bind_model(&self, article: &ArticleGObject) {
        let imp = self.imp();
        let is_same_article = *imp.article_id.read() == article.article_id();

        //log::info!("rebinding same article");

        if article.read() != imp.g_read.get() {
            imp.g_read.set(article.read());
            self.update_title_label_style();
        }

        if article.marked() != imp.g_marked.get() {
            imp.g_marked.set(article.marked());
            self.update_marked();
        }

        if article.tags() != *imp.g_tags.borrow() {
            imp.g_tags.replace(article.tags());
            self.update_tags();
        }

        if article.date() != imp.g_date.get() {
            imp.g_date.set(article.date());
            imp.date_label.set_text(&article.date_string());

            let title = &*article.title();
            if Text2Html::is_html(title) {
                imp.title_label.set_markup(&html2pango::markup(title));
            } else {
                imp.title_label.set_text(title);
            };
            imp.title_label.set_tooltip_text(Some(title));
        }

        if !is_same_article {
            *imp.article_id.write() = article.article_id();
            *imp.feed_id.write() = article.feed_id();

            imp.feed_label.set_text(&*article.feed_title());
            imp.summary_label.set_text(&*article.summary());

            let empty_paintable: Option<&Paintable> = None;

            // clear favicon
            imp.favicon.set_from_icon_name(Some("application-rss+xml-symbolic"));
            // clear thumbnail
            imp.thumb_image.set_from_paintable(empty_paintable);
            imp.summary_label.set_visible(true);
            imp.title_label.set_lines(2);
            imp.thumb_revealer.set_reveal_child(false);

            if imp.right_click_source.read().is_none() {
                self.setup_right_click();
            }

            self.load_favicon();
            self.load_thumbnail();
        }
    }

    fn setup_right_click(&self) {
        let imp = self.imp();

        imp.right_click_source
            .write()
            .replace(imp.row_click.connect_released(clone!(
                @weak self as this => @default-panic, move |_gesture, times, _x, _y|
            {
                if times != 1 {
                    return
                }

                if App::default().content_page_state().read().get_offline() {
                    return
                }

                let imp = this.imp();
                if imp.popover.read().is_none() {
                    imp.popover.write().replace(this.setup_context_popover());
                }

                if let Some(popover) = imp.popover.read().as_ref() {
                    popover.popup();
                };
            })));

        imp.long_press_source
            .write()
            .replace(imp.row_long_press.connect_pressed(clone!(
                @weak self as this => @default-panic, move |_gesture, _x, _y|
            {
                if App::default().content_page_state().read().get_offline() {
                    return
                }

                let imp = this.imp();
                if imp.popover.read().is_none() {
                    imp.popover.write().replace(this.setup_context_popover());
                }

                if let Some(popover) = imp.popover.read().as_ref() {
                    popover.popup();
                };
            })));
    }

    fn setup_context_popover(&self) -> PopoverMenu {
        let imp = self.imp();
        let article_id = imp.article_id.read().clone();

        let model = Menu::new();
        let read_item = MenuItem::new(Some("Toggle Read"), None);
        let star_item = MenuItem::new(Some("Toggle Star"), None);
        let open_item = MenuItem::new(Some("Open in Browser"), None);

        read_item.set_action_and_target_value(Some("win.toggle-article-read"), Some(&article_id.as_str().to_variant()));
        star_item.set_action_and_target_value(
            Some("win.toggle-article-marked"),
            Some(&article_id.as_str().to_variant()),
        );
        open_item.set_action_and_target_value(
            Some("win.open-article-in-browser"),
            Some(&article_id.as_str().to_variant()),
        );

        model.append_item(&read_item);
        model.append_item(&star_item);
        model.append_item(&open_item);

        let popover = PopoverMenu::from_model(Some(&model));
        popover.set_parent(self);
        popover
    }

    fn feed_id(&self) -> FeedID {
        self.imp().feed_id.read().clone()
    }

    fn load_favicon(&self) {
        let scale = GtkUtil::get_scale(self);

        let (oneshot_sender, receiver) = oneshot::channel::<Option<FavIcon>>();
        App::default().load_favicon(self.feed_id(), oneshot_sender);
        let glib_future = receiver.map(clone!(@weak self as this => @default-return (), move |res| match res {
            Ok(Some(icon)) => {
                if let Some(data) = &icon.data {
                    let res = GtkUtil::create_texture_from_bytes(data, 16, 16, scale);
                    if let Ok(texture) = res {
                        let imp = this.imp();
                        imp.favicon.set_from_paintable(Some(&texture));
                    } else if let Err(error) = res {
                        log::debug!("Favicon '{}': {}", icon.feed_id, error);
                    }
                }
            }
            Ok(None) => {
                warn!("Favicon does not contain image data.");
            }
            Err(_) => warn!("Receiving favicon failed."),
        }));
        Util::glib_spawn_future(glib_future);
    }

    fn load_thumbnail(&self) {
        let scale = GtkUtil::get_scale(self);

        if App::default().settings().read().get_article_list_show_thumbs() {
            let (oneshot_sender, receiver) = oneshot::channel::<Option<Thumbnail>>();
            App::default().load_thumbnail(&self.id(), oneshot_sender);
            let glib_future = receiver.map(clone!(@weak self as this => @default-return (), move |res| match res {
                Ok(Some(thumb)) => {
                    let (width, height) = Self::calculate_thumbnail_size(thumb.width, thumb.height, 64);
                    if let Some(data) = &thumb.data {
                        if let Ok(texture) = GtkUtil::create_texture_from_bytes(data, width, height, scale) {
                            let imp = this.imp();
                            imp.thumb_image.set_from_paintable(Some(&texture));
                            imp.summary_label.set_visible(false);
                            imp.title_label.set_lines(3);
                            imp.thumb_revealer.set_reveal_child(true);
                        }
                    }
                }
                Ok(None) => {
                    log::debug!("Thumbnail does not contain image data.");
                }
                Err(_) => warn!("Receiving favicon failed."),
            }));
            Util::glib_spawn_future(glib_future);
        }
    }

    fn calculate_thumbnail_size(thumb_width: Option<i32>, thumb_height: Option<i32>, target_size: i32) -> (i32, i32) {
        if let Some(width) = thumb_width {
            if let Some(height) = thumb_height {
                if width != height {
                    let aspect_ratio = width as f64 / height as f64;
                    if width > height {
                        (target_size, (target_size as f64 / aspect_ratio) as i32)
                    } else {
                        ((target_size as f64 * aspect_ratio) as i32, target_size)
                    }
                } else {
                    (target_size, target_size)
                }
            } else {
                (target_size, target_size)
            }
        } else {
            (target_size, target_size)
        }
    }

    fn update_title_label_style(&self) {
        let imp = self.imp();
        let context = imp.title_label.style_context();
        match imp.g_read.get() {
            GRead::Read => context.remove_class("heading"),
            GRead::Unread => context.add_class("heading"),
        }
    }

    fn update_marked(&self) {
        let imp = self.imp();
        imp.marked.set_visible(imp.g_marked.get() == GMarked::Marked);
    }

    fn update_tags(&self) {
        let imp = self.imp();
        imp.article_tags.update_tags(imp.g_tags.borrow().deref().into());
    }

    pub fn id(&self) -> ArticleID {
        self.imp().article_id.read().clone()
    }

    pub fn read(&self) -> GRead {
        let imp = self.imp();
        imp.g_read.get()
    }

    fn activate(&self) {
        self.emit_by_name::<()>("activated", &[&self.clone()])
    }
}
