use crate::config::{APP_ID, VERSION};
use glib::object::IsA;
use gtk4::prelude::*;
use gtk4::{Application, License, Window};
use libadwaita::prelude::*;
use libadwaita::AboutWindow;

pub const APP_NAME: &'static str = "NewsFlash";
pub const COPYRIGHT: &'static str = "Copyright © 2017-2022 Jan Lukas Gernert";
pub const DESCRIPTION: &'static str = r#"<b>Follow your favorite blogs &amp; news sites.</b>

NewsFlash is an application designed to complement an already existing web-based RSS reader account.

It combines all the advantages of web based services like syncing across all your devices with everything you expect from a modern desktop application.

<b>Features:</b>

 - Desktop notifications
 - Fast search and filtering
 - Tagging
 - Keyboard shortcuts
 - Offline support
 - and much more

<b>Supported Services:</b>

 - Miniflux
 - local RSS
 - fever
 - Fresh RSS
 - NewsBlur
 - Inoreader
 - feedbin
 - Nextcloud News"#;
pub const WEBSITE: &'static str = "https://gitlab.com/news-flash/news_flash_gtk";
pub const ISSUE_TRACKER: &'static str = "https://gitlab.com/news-flash/news_flash_gtk/-/issues";
pub const AUTHORS: &[&'static str] = &["Jan Lukas Gernert", "Felix Bühler", "Alistair Francis"];
pub const DESIGNERS: &[&'static str] = &["Jan Lukas Gernert"];
pub const ARTISTS: &[&'static str] = &["Jakub Steiner"];

pub const RELEASE_NOTES: &'static str = r#"
<p>This release marks the biggest update for NewsFlash so far.</p>
<p>Most importantly the application was ported to Gtk4 &amp; libadwaita. This came along with lots of internal code improvements.</p>
<p>Features:</p>
<ul>
  <li>Refined Interface</li>
  <li>Imporived Phone Layout</li>
  <li>Fresh RSS Support</li>
  <li>Nextcloud News Support</li>
  <li>Reworked Tagging</li>
  <li>Custom Sync Interval</li>
  <li>Sync on Startup</li>
  <li>Hide Articles dated in the Future</li>
  <li>Fullscreen Videos</li>
  <li>Article View: Custom Content Width</li>
  <li>Article View: Custom Line Height</li>
  <li>Article View: Syntax highlighting</li>
  <li>Better Flatpak support via Portals</li>
  <li>Translations</li>
  <li>Keep Articles without Feed</li>
  <li>Option to not sync on metered connections</li>
  <li>Advanced option to disable image auto loading</li>
</ul>
"#;

#[derive(Clone, Debug)]
pub struct NewsFlashAbout;

impl NewsFlashAbout {
    pub fn show<A: IsA<Application> + AdwApplicationExt, W: IsA<Window> + GtkWindowExt>(app: &A, window: &W) {
        let about_window = AboutWindow::builder()
            .application(app)
            .application_name(APP_NAME)
            .transient_for(window)
            .application_icon(APP_ID)
            .developer_name(AUTHORS[0])
            .developers(AUTHORS)
            .designers(DESIGNERS)
            .artists(ARTISTS)
            .license_type(License::Gpl30)
            .version(VERSION)
            .website(WEBSITE)
            .issue_url(ISSUE_TRACKER)
            .comments(DESCRIPTION)
            .copyright(COPYRIGHT)
            .release_notes(RELEASE_NOTES)
            .release_notes_version("2.0")
            .build();
        about_window.show();
    }
}
