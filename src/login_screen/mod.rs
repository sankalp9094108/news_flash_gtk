mod custom_api_secret;
mod password_login;
mod web_login;

use crate::app::App;

pub use self::custom_api_secret::CustomApiSecret;
pub use self::password_login::PasswordLogin;
pub use self::web_login::WebLogin;
use glib::clone;
use gtk4::{prelude::*, subclass::prelude::*, Stack, StackTransitionType};
use news_flash::{
    error::NewsFlashError,
    models::{LoginData, LoginGUI, PluginID, PluginInfo},
    NewsFlash,
};
use parking_lot::RwLock;

#[derive(Debug)]
pub enum LoginPrevPage {
    Welcome,
    Content(PluginID),
    ApiSecret,
}

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct LoginPage {
        pub stack: Stack,
        pub password_login: PasswordLogin,
        pub web_login: WebLogin,
        pub custom_api_secret: CustomApiSecret,

        pub plugin_id: RwLock<Option<PluginID>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LoginPage {
        const NAME: &'static str = "LoginPage";
        type ParentType = gtk4::Box;
        type Type = super::LoginPage;
    }

    impl ObjectImpl for LoginPage {
        fn constructed(&self) {
            self.obj().append(&self.stack);
        }
    }

    impl WidgetImpl for LoginPage {}

    impl BoxImpl for LoginPage {}
}

glib::wrapper! {
    pub struct LoginPage(ObjectSubclass<imp::LoginPage>)
        @extends gtk4::Widget, gtk4::Box;
}

impl LoginPage {
    pub fn new() -> Self {
        glib::Object::new::<Self>()
    }
    pub fn init(&self) {
        let imp = self.imp();

        imp.password_login.init();
        imp.web_login.init();
        imp.custom_api_secret.init();

        self.connect_back_buttons();

        imp.stack.add_named(&imp.password_login, Some("password_login"));
        imp.stack.add_named(&imp.web_login, Some("oauth_login"));
        imp.stack.add_named(&imp.custom_api_secret, Some("custom_api_secret"));

        imp.custom_api_secret
            .connect_submit(clone!(@weak self as this => @default-panic, move |secret| {
                this.imp().web_login.set_custom_api_secret(secret);
                if let Some(plugin_id) = this.imp().plugin_id.read().as_ref() {
                    if let Some(plugin_info) = NewsFlash::list_backends().get(plugin_id) {
                        if let Ok(()) = this.imp().web_login.set_service(plugin_info, LoginPrevPage::ApiSecret) {
                            this.imp().stack.set_transition_type(StackTransitionType::SlideLeft);
                            this.imp().stack.set_visible_child_name("oauth_login")
                        }
                    }
                };
            }));
        imp.custom_api_secret
            .connect_ignore(clone!(@weak self as this => @default-panic, move || {
                this.imp().web_login.set_custom_api_secret(None);
                if let Some(plugin_id) = this.imp().plugin_id.read().as_ref() {
                    if let Some(plugin_info) = NewsFlash::list_backends().get(plugin_id) {
                        if let Ok(()) = this.imp().web_login.set_service(plugin_info, LoginPrevPage::ApiSecret) {
                            this.imp().stack.set_transition_type(StackTransitionType::SlideLeft);
                            this.imp().stack.set_visible_child_name("oauth_login")
                        }
                    }
                };
            }));
    }

    pub fn reset(&self) {
        let imp = self.imp();
        imp.password_login.reset();
        imp.web_login.reset();
        imp.custom_api_secret.reset();
    }

    pub fn set_service(&self, info: &PluginInfo, prev_page: LoginPrevPage, data: Option<LoginData>) {
        let imp = self.imp();

        imp.plugin_id.write().replace(info.id.clone());

        match &info.login_gui {
            LoginGUI::OAuth(oauth_info) => {
                if oauth_info.custom_api_secret {
                    imp.custom_api_secret.set_service(info, prev_page);

                    if let Some(LoginData::OAuth(oauth_data)) = data {
                        if let Some(api_secret) = &oauth_data.custom_api_secret {
                            imp.custom_api_secret.fill(api_secret);
                        } else {
                            imp.custom_api_secret.reset();
                        }
                    }

                    imp.stack.set_transition_type(StackTransitionType::None);
                    imp.stack.set_visible_child_name("custom_api_secret")
                } else if let Ok(()) = imp.web_login.set_service(info, prev_page) {
                    imp.stack.set_transition_type(StackTransitionType::None);
                    imp.stack.set_visible_child_name("oauth_login")
                }
            }
            LoginGUI::Direct(_) => {
                if let Ok(()) = imp.password_login.set_service(info, prev_page) {
                    if let Some(LoginData::Direct(data)) = data {
                        imp.password_login.fill(data);
                    } else {
                        imp.password_login.reset();
                    }
                    imp.stack.set_transition_type(StackTransitionType::None);
                    imp.stack.set_visible_child_name("password_login")
                }
            }
            LoginGUI::None => {}
        }
    }

    pub fn show_error(&self, error: NewsFlashError, data: &LoginData) {
        let imp = self.imp();

        match &data {
            LoginData::OAuth(_) => imp.web_login.show_error(error),
            LoginData::Direct(_) => imp.password_login.show_error(error),
            LoginData::None(_) => {}
        }
    }

    fn connect_back_buttons(&self) {
        let imp = self.imp();

        imp.web_login
            .connect_back(clone!(@weak self as this => @default-panic, move |prev_page| {
                match prev_page {
                    Some(LoginPrevPage::Content(_id)) => App::default().main_window().show_content_page(),
                    Some(LoginPrevPage::ApiSecret) => {
                        this.imp().stack.set_transition_type(StackTransitionType::SlideRight);
                        this.imp().stack.set_visible_child_name("custom_api_secret")
                    },
                    Some(LoginPrevPage::Welcome) | None => App::default().main_window().show_welcome_page(),
                }
            }));
        imp.password_login
            .connect_back(clone!(@weak self as this => @default-panic, move |prev_page| {
                match prev_page {
                    Some(LoginPrevPage::Content(_id)) => App::default().main_window().show_content_page(),
                    Some(_) | None => App::default().main_window().show_welcome_page(),
                }
            }));
        imp.custom_api_secret
            .connect_back(clone!(@weak self as this => @default-panic, move |prev_page| {
                match prev_page {
                    Some(LoginPrevPage::Content(_id)) => App::default().main_window().show_content_page(),
                    Some(_) | None => App::default().main_window().show_welcome_page(),
                }
            }));
    }
}
