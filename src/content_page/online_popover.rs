use gtk4::{prelude::*, subclass::prelude::*, Label, Popover, Widget};

mod imp {
    use super::*;

    pub struct OnlinePopover {
        pub label: Label,
    }

    impl Default for OnlinePopover {
        fn default() -> Self {
            let label = Label::new(Some("You are back online."));
            label.set_margin_top(10);
            label.set_margin_bottom(10);
            label.set_margin_start(10);
            label.set_margin_end(10);
            Self { label }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for OnlinePopover {
        const NAME: &'static str = "OnlinePopover";
        type ParentType = Popover;
        type Type = super::OnlinePopover;
    }

    impl ObjectImpl for OnlinePopover {
        fn constructed(&self) {
            self.obj().set_child(Some(&self.label));
        }
    }

    impl WidgetImpl for OnlinePopover {}

    impl PopoverImpl for OnlinePopover {}
}

glib::wrapper! {
    pub struct OnlinePopover(ObjectSubclass<imp::OnlinePopover>)
        @extends Widget, Popover;
}

impl OnlinePopover {
    pub fn new() -> Self {
        glib::Object::new::<Self>()
    }
}
