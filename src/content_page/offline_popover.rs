use gtk4::{prelude::*, subclass::prelude::*, Label, Popover, Widget};

mod imp {
    use super::*;

    pub struct OfflinePopover {
        pub label: Label,
    }

    impl Default for OfflinePopover {
        fn default() -> Self {
            let label = Label::new(Some("You are now offline.\nClick here to go back online."));
            label.set_margin_top(10);
            label.set_margin_bottom(10);
            label.set_margin_start(10);
            label.set_margin_end(10);

            Self { label }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for OfflinePopover {
        const NAME: &'static str = "OfflinePopover";
        type ParentType = Popover;
        type Type = super::OfflinePopover;
    }

    impl ObjectImpl for OfflinePopover {
        fn constructed(&self) {
            self.obj().set_child(Some(&self.label));
        }
    }

    impl WidgetImpl for OfflinePopover {}

    impl PopoverImpl for OfflinePopover {}
}

glib::wrapper! {
    pub struct OfflinePopover(ObjectSubclass<imp::OfflinePopover>)
        @extends Widget, Popover;
}

impl OfflinePopover {
    pub fn new() -> Self {
        glib::Object::new::<Self>()
    }
}
