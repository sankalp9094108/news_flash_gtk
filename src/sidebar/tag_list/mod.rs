pub mod models;
mod tag_row;

use crate::sidebar::SidebarIterateItem;
use diffus::edit::{collection, Edit};
use gio::ListStore;
use glib::clone;
use gtk4::{
    prelude::*, subclass::prelude::*, Box, CompositeTemplate, ListView, SignalListItemFactory, SingleSelection, Widget,
};
use gtk4::{ConstantExpression, ListItem, PropertyExpression};
use models::{TagGObject, TagListModel};
use news_flash::models::TagID;
use parking_lot::RwLock;
use std::collections::HashMap;
use tag_row::TagRow;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/tag_list.ui")]
    pub struct TagList {
        #[template_child]
        pub listview: TemplateChild<ListView>,
        #[template_child]
        pub factory: TemplateChild<SignalListItemFactory>,
        #[template_child]
        pub selection: TemplateChild<SingleSelection>,
        #[template_child]
        pub list_store: TemplateChild<ListStore>,

        pub list_model: RwLock<TagListModel>,
        pub model_index: RwLock<HashMap<TagID, TagGObject>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TagList {
        const NAME: &'static str = "TagList";
        type ParentType = Box;
        type Type = super::TagList;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for TagList {}

    impl WidgetImpl for TagList {}

    impl BoxImpl for TagList {}
}

glib::wrapper! {
    pub struct TagList(ObjectSubclass<imp::TagList>)
        @extends Widget, Box;
}

impl TagList {
    pub fn new() -> Self {
        glib::Object::new::<Self>()
    }

    pub fn init(&self) {
        let imp = self.imp();

        imp.factory.connect_setup(
            clone!(@weak self as this => @default-panic, move |_factory, list_item| {
                let row = TagRow::new();
                list_item.set_child(Some(&row));

                row.connect_local("activated", false, clone!(@weak this => @default-panic, move |_args| {
                    let imp = this.imp();
                    imp.listview.emit_by_name::<()>("activate", &[&imp.selection.selected()]);
                    None
                }));

                // Create expression describing `list_item->item`
                let list_item_expression = ConstantExpression::new(list_item);
                let article_gobject_expression =
                    PropertyExpression::new(ListItem::static_type(), Some(&list_item_expression), "item");

                // Update title
                let read_expression =
                    PropertyExpression::new(TagGObject::static_type(), Some(&article_gobject_expression), "title");
                read_expression.bind(&row, "title", Some(&row));

                // Update color
                let read_expression =
                    PropertyExpression::new(TagGObject::static_type(), Some(&article_gobject_expression), "color");
                read_expression.bind(&row, "color", Some(&row));
            }),
        );
        imp.factory.connect_bind(|_factory, list_item| {
            let tag = list_item.item().unwrap().downcast::<TagGObject>().unwrap();
            let child = list_item.child().unwrap().downcast::<TagRow>().unwrap();
            child.bind_model(&tag);
        });
    }

    fn is_empty(&self) -> bool {
        let imp = self.imp();
        imp.list_store.n_items() == 0
    }

    pub fn listview(&self) -> &ListView {
        let imp = self.imp();
        &imp.listview
    }

    pub fn update(&self, new_list: TagListModel) {
        let imp = self.imp();

        let mut old_list = new_list;
        std::mem::swap(&mut old_list, &mut imp.list_model.write());

        old_list.sort();
        imp.list_model.write().sort();

        let model_guard = imp.list_model.read();
        let diff = old_list.generate_diff(&model_guard);
        let mut pos = 0;

        match diff {
            Edit::Copy(_list) => {
                // no difference
                return;
            }
            Edit::Change(diff) => {
                let _ = diff
                    .into_iter()
                    .map(|edit| {
                        match edit {
                            collection::Edit::Copy(_tag) => {
                                // nothing changed
                                pos += 1;
                            }
                            collection::Edit::Insert(tag) => {
                                let tag_id = tag.id.clone();
                                let tag_gobject = TagGObject::from_model(tag);
                                imp.list_store.insert(pos as u32, &tag_gobject);
                                imp.model_index.write().insert(tag_id, tag_gobject);
                                pos += 1;
                            }
                            collection::Edit::Remove(tag) => {
                                let remove_pos = imp
                                    .model_index
                                    .read()
                                    .get(&tag.id)
                                    .and_then(|tag_gobject| imp.list_store.find(tag_gobject));
                                if let Some(remove_pos) = remove_pos {
                                    imp.list_store.remove(remove_pos);
                                    imp.model_index.write().remove(&tag.id);
                                }
                            }
                            collection::Edit::Change(diff) => {
                                if let Some(tag_gobject) = imp.model_index.read().get(&diff.id) {
                                    if let Some(label) = diff.label {
                                        tag_gobject.set_title(label);
                                    }
                                    if let Some(color) = diff.color {
                                        tag_gobject.set_color(Some(color));
                                    }
                                }
                                pos += 1;
                            }
                        }
                    })
                    .collect::<Vec<_>>();
            }
        };
    }

    pub fn selection(&self) -> &SingleSelection {
        let imp = self.imp();
        &imp.selection
    }

    pub fn get_selected_tag_model(&self) -> Option<TagGObject> {
        if !self.is_empty() {
            let imp = self.imp();
            let selected_row_pos = imp.selection.selected();
            imp.list_store
                .item(selected_row_pos)
                .and_then(|obj| obj.downcast::<TagGObject>().ok())
        } else {
            None
        }
    }

    pub fn get_selection(&self) -> Option<(TagID, String)> {
        self.get_selected_tag_model().map(|tag| (tag.tag_id(), tag.title()))
    }

    pub fn get_next_item(&self) -> SidebarIterateItem {
        let imp = self.imp();

        if let Some(tag) = self.get_selected_tag_model() {
            return imp.list_model.read().calculate_next_item(&tag.tag_id());
        }
        SidebarIterateItem::NothingSelected
    }

    pub fn get_prev_item(&self) -> SidebarIterateItem {
        let imp = self.imp();

        if let Some(tag) = self.get_selected_tag_model() {
            return imp.list_model.read().calculate_prev_item(&tag.tag_id());
        }
        SidebarIterateItem::NothingSelected
    }

    pub fn get_first_item(&self) -> Option<TagID> {
        let imp = self.imp();
        imp.list_model.read().first().map(|model| model.id)
    }

    pub fn get_last_item(&self) -> Option<TagID> {
        let imp = self.imp();
        imp.list_model.read().last().map(|model| model.id)
    }

    pub fn set_selection(&self, selection: TagID) {
        let imp = self.imp();
        if let Some(tag_gobject) = imp.model_index.read().get(&selection) {
            if let Some(pos) = imp.list_store.find(tag_gobject) {
                imp.selection.set_selected(pos);
            }
        }
    }
}
