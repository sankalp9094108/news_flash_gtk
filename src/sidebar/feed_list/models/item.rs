use super::category::FeedListCategoryModel;
use super::feed::FeedListFeedModel;
use diffus::{Diffus, Same};
use news_flash::models::{CategoryID, FeedID, FeedMapping};
use std::{cmp::Ordering, sync::Arc};

#[derive(Eq, Clone, Debug, Hash)]
pub enum FeedListItemID {
    Category(CategoryID),
    Feed(FeedID, Arc<FeedMapping>),
}

impl PartialEq for FeedListItemID {
    fn eq(&self, other: &FeedListItemID) -> bool {
        match other {
            FeedListItemID::Category(other_category) => match self {
                FeedListItemID::Category(self_category) => other_category == self_category,
                FeedListItemID::Feed(_, _) => false,
            },
            FeedListItemID::Feed(other_feed, other_mapping) => match self {
                FeedListItemID::Feed(self_feed, self_parent) => other_feed == self_feed && other_mapping == self_parent,
                FeedListItemID::Category(_) => false,
            },
        }
    }
}

#[derive(Diffus, Eq, Clone, Debug)]
pub enum FeedListItem {
    Feed(FeedListFeedModel),
    Category(FeedListCategoryModel),
}

impl Same for FeedListItem {
    fn same(&self, other: &Self) -> bool {
        match self {
            FeedListItem::Feed(self_feed) => match other {
                FeedListItem::Feed(other_feed) => self_feed.same(&other_feed),
                FeedListItem::Category(_other_category) => false,
            },
            FeedListItem::Category(self_category) => match other {
                FeedListItem::Feed(_other_feed) => false,
                FeedListItem::Category(other_category) => self_category.same(&other_category),
            },
        }
    }
}

impl PartialEq for FeedListItem {
    fn eq(&self, other: &FeedListItem) -> bool {
        match other {
            FeedListItem::Category(other_category) => match self {
                FeedListItem::Category(self_category) => other_category.id == self_category.id,
                FeedListItem::Feed(_) => false,
            },
            FeedListItem::Feed(other_feed) => match self {
                FeedListItem::Feed(self_feed) => other_feed.id == self_feed.id,
                FeedListItem::Category(_) => false,
            },
        }
    }
}

impl Ord for FeedListItem {
    fn cmp(&self, other: &FeedListItem) -> Ordering {
        match self {
            FeedListItem::Feed(self_feed) => match other {
                FeedListItem::Feed(other_feed) => self_feed.sort_index.cmp(&other_feed.sort_index),
                FeedListItem::Category(other_category) => self_feed.sort_index.cmp(&other_category.sort_index),
            },
            FeedListItem::Category(self_category) => match other {
                FeedListItem::Feed(other_feed) => self_category.sort_index.cmp(&other_feed.sort_index),
                FeedListItem::Category(other_category) => self_category.sort_index.cmp(&other_category.sort_index),
            },
        }
    }
}

impl PartialOrd for FeedListItem {
    fn partial_cmp(&self, other: &FeedListItem) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
